﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AESMS
{
    public partial class Formulaire_Recherche : Form
    {
        public Formulaire_Recherche()
        {
            InitializeComponent();
        }

        private void Formulaire_Recherche_Load(object sender, EventArgs e)
        {
            //
            IFormatter format = new BinaryFormatter();
            if (System.IO.File.Exists("MembreVeiw.aesms"))
            {
                System.IO.File.Delete("MembreVeiw.aesms");
            }

            if (System.IO.File.Exists("Membre.aesms"))
            {
                using (Stream flux = new FileStream("Membre.aesms", FileMode.Open, FileAccess.Read))
                {
                    List<Membre> Liste_Lecture;
                    Liste_Lecture = (List<Membre>)format.Deserialize(flux);
                    int i = 1;
                    foreach (Membre m in Liste_Lecture)
                    {
                        if(m.getActive())
                        {
                            comboBox_ListeMembre.Items.Add("" + i + " - " + m.getNom() + " " + m.getPrenom() + " " + m.getAge() + " Ans");
                            i++;
                        }
                        
                    }
                }
            }
            else
            {
                comboBox_ListeMembre.Items.Add("Il n'y a aucun membre ajouté dans le système !");
            }
            
                
        }

        private void comboBox_ListeMembre_SelectedIndexChanged(object sender, EventArgs e)
        {
            string ch = "", str = comboBox_ListeMembre.Text;
            int cpt = 0;
            for(int i=0; i<str.Length;i++)
            {
                if (str[i] == ' ' && cpt<3)
                    cpt++;
                if (cpt > 2)
                {
                    if (str[i] == ' ')
                    {
                        if (str[i + 1] >= '0' && str[i + 1] <= '9')
                            break;
                    }
                        
                    ch += str[i];
                }
                if (cpt == 2)
                    cpt++;
                
            }
            textBox_Recherche.Text = ch;
        }
        static void savOne(List<Membre> Liste_Trouver)
        {
            IFormatter format = new BinaryFormatter();
            using (Stream flux = new FileStream("MembreVeiw.aesms", FileMode.Create, FileAccess.Write))
            {
                format.Serialize(flux, Liste_Trouver);
            }
        }

        static bool find(string ch,string ch2)
        {
            bool ok = false;
            string max, min;
            if(ch.Length>ch2.Length)
            {
                max = ch;
                min = ch2;
            }
            else
            {
                max = ch2;
                min = ch;
            }
            for(int i=0;i<max.Length;i++)
            {
                string str = "";
                for(int j=0;j<min.Length;j++)
                {
                    if(i+min.Length<=max.Length)
                    {
                        str += max[i + j];
                    }
                }
                if(str.Equals(min))
                {
                    ok = true;
                    break;
                }
           
            }
            return ok;
        }
        private void button_Rechercher_Click(object sender, EventArgs e)
        {

            if(textBox_Recherche.Text=="")
            {
                MessageBox.Show("Veillez saisir ou choisir une valeur de recherche avant de continuer", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else
            {
                
                IFormatter format = new BinaryFormatter();
                string recherche = textBox_Recherche.Text;
                bool trouve = false;

                using (Stream flux = new FileStream("Membre.aesms", FileMode.Open, FileAccess.Read))
                {
                    List<Membre> Liste_Lecture ;
                    List<Membre> Liste_Trouver = new List<Membre>();
                    if(System.IO.File.Exists("MembreVeiw.aesms"))
                    {
                        System.IO.File.Delete("MembreVeiw.aesms");
                    }

                    Liste_Lecture = (List<Membre>)format.Deserialize(flux);
                    foreach (Membre m in Liste_Lecture)
                    {
                        if(m.getActive())
                        {
                            if (find(recherche, m.getNom()) && find(recherche, m.getPrenom()))
                            {
                                Liste_Trouver.Add(m);
                                trouve = true;
                            }
                            else
                            {
                                if (find(recherche, m.getNom()))
                                {
                                    Liste_Trouver.Add(m);
                                    trouve = true;
                                }
                                else
                                {
                                    if (find(recherche, m.getPrenom()))
                                    {
                                        Liste_Trouver.Add(m);
                                        trouve = true;
                                    }
                                    else
                                    {
                                        if (find(recherche, m.getDateNaissance()))
                                        {
                                            Liste_Trouver.Add(m);
                                            trouve = true;
                                        }
                                        else
                                        {
                                            if (find(recherche, m.getSexe()))
                                            {
                                                Liste_Trouver.Add(m);
                                                trouve = true;
                                            }
                                            else
                                            {
                                                if (find(recherche, m.getFormation()))
                                                {
                                                    Liste_Trouver.Add(m);
                                                    trouve = true;
                                                }
                                                else
                                                {
                                                    if (find(recherche, m.getAdresse()))
                                                    {
                                                        Liste_Trouver.Add(m);
                                                        trouve = true;
                                                    }
                                                    else
                                                    {
                                                        if (find(recherche, m.getTelephone()))
                                                        {
                                                            Liste_Trouver.Add(m);
                                                            trouve = true;
                                                        }
                                                        else
                                                        {
                                                            if (find(recherche, m.getCode()))
                                                            {
                                                                Liste_Trouver.Add(m);
                                                                trouve = true;
                                                            }
                                                            else
                                                            {
                                                                if (find(recherche, "" + m.getAge()))
                                                                {
                                                                    Liste_Trouver.Add(m);
                                                                    trouve = true;
                                                                }
                                                                else
                                                                {

                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        
                    }
                    if(trouve)
                    {
                        savOne(Liste_Trouver);
                        this.Hide();
                        Formulaire_AfficherMembre afficher = new Formulaire_AfficherMembre();
                        afficher.ShowDialog();
                        this.Show();
                        //
                    }
                    
                    else
                        MessageBox.Show("Désolé, ce membre n'existe pas dans le système !", "Resultat Echec", MessageBoxButtons.OK, MessageBoxIcon.Information);




                }
            }
        }

        private void button_Retour_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button_Main_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }
    }
}
