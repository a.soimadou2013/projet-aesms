﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AESMS
{
    public partial class Formulaire_AjoutMembre : Form
    {
        public Formulaire_AjoutMembre()
        {
            InitializeComponent();
        }

       

        
        bool choisirImage = false;
        
        bool genererCode=true;

        bool Ajout_Effectuer = false;

        int nombreDeClicPivoter = 1;

        string code="";



        private void pictureBox_Photo_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            //open.Filter = "Bitmaps|*.bmp|jpeps|*.jpg";

            if (open.ShowDialog() == DialogResult.OK)
            {
                choisirImage = true;
                pictureBox_Photo.ImageLocation = open.FileName;
            }
            
        }


        private void button_Pivoter_Click(object sender, EventArgs e)
        {
            if(choisirImage)
            {
                Image img = pictureBox_Photo.Image;

                switch (nombreDeClicPivoter)
                {
                    case 1:
                        img.RotateFlip(RotateFlipType.Rotate90FlipNone);
                        break;
                    case 2:
                        img.RotateFlip(RotateFlipType.Rotate180FlipNone);
                        break;
                    case 3:
                        img.RotateFlip(RotateFlipType.Rotate270FlipNone);
                        nombreDeClicPivoter = 1;
                        break;


                }
                nombreDeClicPivoter++;
                pictureBox_Photo.Image = img;
            }
            else
            {
                MessageBox.Show("Désolé, veillez charger une image avant de continuer", "Opération Impossible", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            
        }

        private void button_NouvelleAjout_Click(object sender, EventArgs e)
        {
            textBox_Nom.Clear();
            textBox_Prenom.Clear();
            dateTimePicker_DateNaissance.ResetText();
            comboBox_Sexe.SelectedIndex = 0;
            textBox_Formation.Clear();
            textBox_Adresse.Clear();
            textBox_Telephone.Clear();
            textBox_Nom.Focus();
            genererCode = true;
            Ajout_Effectuer = false;
            pictureBox_Photo.Image = null;
        }

        static bool Verif_Saisie(string ch)
        {
            bool ok = true;
            if (ch.Length == 0)
                ok = false;
            
            return ok;
        }
        static Int32 position(Int32 o)
        {
            Int32 marge = 38;
            return ((o * marge) / 100);
        }

        private void button_PreviewPrint_Click(object sender, EventArgs e)
        {
            //printDocument_CarteAdhesion.DefaultPageSettings.Landscape = true;
            printPreviewDialog_CarteAdhesion.Document = printDocument_CarteAdhesion;

            printPreviewDialog_CarteAdhesion.ShowDialog();
        }

        private void printPreviewDialog_CarteAdhesion_Load(object sender, EventArgs e)
        {

        }

        private void printDocument_CarteAdhesion_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Bitmap bmp = Properties.Resources.carte_MODEL21;
            Image newImage = bmp, m = pictureBox_Photo.Image;
            // printDocument_CarteAdhesion.DefaultPageSettings.Landscape = true;

            Int32 taille = position(30);

            e.Graphics.DrawImage(newImage, 0, 0, position(newImage.Width), position(newImage.Height));

            //Verifie si toute les champs sont saisie
            bool ok = false;
            if (Verif_Saisie(textBox_Nom.Text))
            {
                if (Verif_Saisie(textBox_Prenom.Text))
                {
                    if (Verif_Saisie(dateTimePicker_DateNaissance.Text))
                    {
                        if (!comboBox_Sexe.Text.Equals("- Choisissez ici"))
                        {
                            if (Verif_Saisie(textBox_Formation.Text))
                            {
                                if (Verif_Saisie(textBox_Adresse.Text))
                                {
                                    if (Verif_Saisie(textBox_Telephone.Text))
                                    {
                                        ok = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }


            if (ok)
            {
                if (choisirImage)
                    e.Graphics.DrawImage(m, 299, 69, 82, 79);

                e.Graphics.DrawString(textBox_Nom.Text, new Font("MV Boli", taille, FontStyle.Regular), Brushes.WhiteSmoke, new Point(position(215), position(139)));
                e.Graphics.DrawString(textBox_Prenom.Text, new Font("MV Boli", taille, FontStyle.Regular), Brushes.WhiteSmoke, new Point(position(225), position(206)));
                e.Graphics.DrawString(dateTimePicker_DateNaissance.Text, new Font("MV Boli", taille, FontStyle.Regular), Brushes.WhiteSmoke, new Point(position(310), position(271)));
                e.Graphics.DrawString("Comorienne", new Font("MV Boli", taille, FontStyle.Regular), Brushes.WhiteSmoke, new Point(position(300), position(338)));
                e.Graphics.DrawString(textBox_Formation.Text, new Font("MV Boli", taille, FontStyle.Regular), Brushes.WhiteSmoke, new Point(position(300), position(399)));
                e.Graphics.DrawString(textBox_Telephone.Text, new Font("MV Boli", taille, FontStyle.Regular), Brushes.WhiteSmoke, new Point(position(240), position(461)));
                e.Graphics.DrawString( "Avant "+(DateTime.Now.Day + "/" + DateTime.Now.Month + "/" + (DateTime.Now.Year + 1)), new Font("MV Boli", taille, FontStyle.Regular), Brushes.WhiteSmoke, new Point(position(220), position(553)));

                if (genererCode)
                {
                    string str = textBox_Prenom.Text;
                    if (str.Length >= 2)
                    {
                        code = "";
                        int nbAleatoire = new Random().Next(1, DateTime.Now.Year), v = 0;
                        for (int i = 0; i < 2; i++)
                        {
                            code += str[i];
                        }
                        v += DateTime.Now.Year + DateTime.Now.Day + DateTime.Now.Month;
                        v -= nbAleatoire;
                        code += v;
                        code += "COM";
                        genererCode = false;
                    }
                    else
                    {
                        MessageBox.Show("Votre prénon est trop courte , veillez saisir au moins 2 caractère !", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        printPreviewDialog_CarteAdhesion.Close();
                        textBox_Prenom.Clear();
                        textBox_Prenom.Focus();
                    }
                }


                e.Graphics.DrawString(code, new Font("MV Boli", taille, FontStyle.Regular), Brushes.WhiteSmoke, new Point(position(720), position(554)));

                
            }
            else
            {
                MessageBox.Show("Veillez remplir toutes les champs avant de continuer !", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                printPreviewDialog_CarteAdhesion.Close();

            }

        }

        private void button_Ajouter_Click(object sender, EventArgs e)
        {
            bool ok = false;
            string patch = "Membre.aesms";
            Membre m = new Membre();
            if (Verif_Saisie(textBox_Nom.Text))
            {
                if (Verif_Saisie(textBox_Prenom.Text))
                {
                    if (Verif_Saisie(dateTimePicker_DateNaissance.Text))
                    {
                        if (!comboBox_Sexe.Text.Equals("- Choisissez ici"))
                        {
                            if (Verif_Saisie(textBox_Formation.Text))
                            {
                                if (Verif_Saisie(textBox_Adresse.Text))
                                {
                                    if (Verif_Saisie(textBox_Telephone.Text))
                                    {
                                        ok = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (!ok)
            {
                MessageBox.Show("Veillez remplir toutes les champs avant de continuer !", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (genererCode)
                {
                    string str = textBox_Prenom.Text;
                    if(str.Length>=2)
                    {
                        code = "";
                        int nbAleatoire = new Random().Next(1, DateTime.Now.Year), v = 0;
                        for (int i = 0; i < 2; i++)
                        {
                            code += str[i];
                        }
                        v += DateTime.Now.Year + DateTime.Now.Day + DateTime.Now.Month;
                        v -= nbAleatoire;
                        code += v;
                        code += "COM";
                        genererCode = false;


                        m.setCode(code);
                        m.setNom(textBox_Nom.Text);
                        m.setPrenom(textBox_Prenom.Text);
                        m.setDateNaissance(dateTimePicker_DateNaissance.Text);
                        m.setSexe(comboBox_Sexe.Text);
                        m.setFormation(textBox_Formation.Text);
                        m.setAdresse(textBox_Adresse.Text);
                        m.setTelephone(textBox_Telephone.Text);
                        if (choisirImage)
                        {
                            m.setImage(m.getCode());
                        }
                        else
                            m.setImage("");



                        List<Membre> ListeMembre = new List<Membre>();


                        IFormatter format = new BinaryFormatter();
                        bool trouve = false,ajout_Carte=false;
                        
                        if (System.IO.File.Exists(patch))
                        {

                            using (Stream flux = new FileStream(patch, FileMode.Open, FileAccess.Read))
                            {
                                List<Membre> Liste_Lecture;
                                Liste_Lecture = (List<Membre>)format.Deserialize(flux);
                                ListeMembre = Liste_Lecture;
                                foreach (Membre m2 in Liste_Lecture)
                                {
                                    if (m2.getTelephone() == m.getTelephone() && m.getActive() && m2.getActive())
                                    {
                                        if (m2.getNom() == m.getNom() && m2.getPrenom() == m.getPrenom())
                                        {
                                            Ajout_Effectuer = false;
                                            trouve = true;
                                            button_NouvelleAjout_Click(sender, e);
                                            MessageBox.Show("Désolé, ce membre existe déja dans le système !", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                            break;
                                        }

                                    }
                                }

                            }
                        }
                        else
                        {
                            using (Stream flux = new FileStream(patch, FileMode.OpenOrCreate, FileAccess.Write))
                            {
                                format.Serialize(flux, ListeMembre);
                            }

                            bool ok2 = false;
                            List<Membre> ecriture = new List<Membre>();
                            if (System.IO.File.Exists("Liste_Carte.aesms"))
                            {
                                List<Membre> lecture = new List<Membre>();


                                using (Stream flux = new FileStream("Liste_Carte.aesms", FileMode.Open, FileAccess.Read))
                                {
                                    ecriture = lecture = (List<Membre>)format.Deserialize(flux);
                                }
                                foreach (Membre m2 in lecture)
                                {
                                    if (m.Equals(m2) && m2.getActive())
                                    {
                                        ok2 = true;
                                        break;
                                    }
                                }
                                if (ok2)
                                {
                                    MessageBox.Show("Désolé, ce membre existe déja dans la liste d'attente d'impression !", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

                                }
                                else
                                {
                                    if (System.IO.File.Exists("Liste_Carte.aesms"))
                                        System.IO.File.Delete("Liste_Carte.aesms");
                                    ecriture.Add(m);
                                    using (Stream flux = new FileStream("Liste_Carte.aesms", FileMode.OpenOrCreate, FileAccess.Write))
                                    {
                                        format.Serialize(flux, ecriture);
                                    }
                                    MessageBox.Show("Ajout éffectué avec succes dans la liste d'impression de carte d'adhésion !", "Ajout effectué", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    ajout_Carte = true;
                                }
                            }
                            else
                            {
                                ecriture.Add(m);
                                using (Stream flux = new FileStream("Liste_Carte.aesms", FileMode.OpenOrCreate, FileAccess.Write))
                                {
                                    format.Serialize(flux, ecriture);
                                }
                                MessageBox.Show("Ajout éffectué avec succes dans la liste d'impression de carte d'adhésion !", "Ajout effectué", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                ajout_Carte = true;
                            }


                            Ajout_Effectuer = true;
                        }
                        if (trouve==false)
                        {
                            ListeMembre.Add(m);
                            using (Stream flux = new FileStream(patch, FileMode.OpenOrCreate, FileAccess.Write))
                            {
                                format.Serialize(flux, ListeMembre);
                            }

                            bool ok2 = false;
                            List<Membre> ecriture = new List<Membre>();
                            if (System.IO.File.Exists("Liste_Carte.aesms") && ajout_Carte ==false)
                            {
                                List<Membre> lecture = new List<Membre>();


                                using (Stream flux = new FileStream("Liste_Carte.aesms", FileMode.Open, FileAccess.Read))
                                {
                                    ecriture = lecture = (List<Membre>)format.Deserialize(flux);
                                }
                                foreach (Membre m2 in lecture)
                                {
                                    if (m.Equals(m2) && m2.getActive())
                                    {
                                        ok = true;
                                        break;
                                    }
                                }
                                if (ok2)
                                {
                                    MessageBox.Show("Désolé, ce membre existe déja dans la liste d'attente d'impression !", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

                                }
                                else
                                {
                                    if (System.IO.File.Exists("Liste_Carte.aesms"))
                                        System.IO.File.Delete("Liste_Carte.aesms");
                                    ecriture.Add(m);
                                    using (Stream flux = new FileStream("Liste_Carte.aesms", FileMode.OpenOrCreate, FileAccess.Write))
                                    {
                                        format.Serialize(flux, ecriture);
                                    }
                                    MessageBox.Show("Ajout éffectué avec succes dans la liste d'impression de carte d'adhésion !", "Ajout effectué", MessageBoxButtons.OK, MessageBoxIcon.Information);

                                }
                            }
                            

                            Ajout_Effectuer = true;
                        }

                        if (Ajout_Effectuer)
                        {
                            System.IO.Directory.CreateDirectory("Image_Membre");
                            string adresse = "";
                            if (choisirImage)
                            {
                                string image = @"Image_Membre\" + m.getCode();
                                adresse = image;
                                pictureBox_Photo.Image.Save(adresse + ".jpg");
                            }


                            MessageBox.Show("Membre ajouté avec succès !", "Ajout éffectué", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }

                    }
                    else
                    {
                        MessageBox.Show("Votre prénon est trop courte , veillez saisir au moins 2 caractère !", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        textBox_Prenom.Focus();
                        textBox_Prenom.Clear();
                    }
                    

                }

              
            }
            button_NouvelleAjout_Click(sender, e);
        }

        private void Formulaire_AjoutMembre_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }

        private void comboBox_Sexe_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Formulaire_AjoutMembre_Load(object sender, EventArgs e)
        {
            button_NouvelleAjout_Click(sender, e);
            //
        }

        private void label2_Click(object sender, EventArgs e)
        {
            pictureBox_Photo_Click(sender, e);
        }

        private void button_Retour_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button_Main_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }
    }
}
