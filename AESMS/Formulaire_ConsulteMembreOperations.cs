﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AESMS
{
    public partial class Formulaire_ConsulteMembreOperations : Form
    {
        public Formulaire_ConsulteMembreOperations()
        {
            InitializeComponent();
        }

        List<Operation> Liste_Lecture;

        int position = 0, nb = 0;
        Membre membreTrouve = new Membre();
        
        
        

        private void button_Retour_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button_Main_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        private void button_Precedant_Click_1(object sender, EventArgs e)
        {
            int i = 0;

            List<Operation> Lecture = Liste_Lecture;
            foreach (Operation op in Lecture)
            {

                if (i == position + 1)
                {

                    textBox_ID.Text = "" + op.getId();
                    textBox_Date.Text = op.getDate().Day + "/" + op.getDate().Month + "/" + op.getDate().Year;
                    string type = "";
                    switch (op.getTypeOperation())
                    {
                        case 1:
                            type = "Dépot";
                            break;
                        case 2:
                            type = "Emprunt";
                            break;
                        case 3:
                            type = "Reboursement d'emprunt";
                            break;
                        case 4:
                            type = "Frais d'un événement";
                            break;
                        case 5:
                            type = "Autres frais supplémentaires";
                            break;
                        case 0:
                            type = "Aucune opération";
                            break;
                    }
                    textBox_TypeOperation.Text = type;
                    textBox_MontantOperation.Text = "" + op.getMontantOperation();
                    textBox_Restant.Text = "" + op.getMontantRestant();
                    membreTrouve = op.getMembre();
                    

                    position = i;

                    break;
                }

                i++;
            }

            if (position >= nb)
                button_Precedant.Visible = false;

            if (position <= 0)
            {
                button_Suivant.Visible = false;
            }
            else
            {
                button_Suivant.Visible = true;
            }
        }

        private void button_Suivant_Click_1(object sender, EventArgs e)
        {
            int i = 0;
            List<Operation> Lecture = Liste_Lecture;
            foreach (Operation op in Lecture)
            {

                if (i == position - 1)
                {
                    textBox_ID.Text = "" + op.getId();
                    textBox_Date.Text = op.getDate().Day + "/" + op.getDate().Month + "/" + op.getDate().Year;
                    string type = "";
                    switch (op.getTypeOperation())
                    {
                        case 1:
                            type = "Dépot";
                            break;
                        case 2:
                            type = "Emprunt";
                            break;
                        case 3:
                            type = "Reboursement d'emprunt";
                            break;
                        case 4:
                            type = "Frais d'un événement";
                            break;
                        case 5:
                            type = "Autres frais supplémentaires";
                            break;
                        case 0:
                            type = "Aucune opération";
                            break;
                    }
                    textBox_TypeOperation.Text = type;
                    textBox_MontantOperation.Text = "" + op.getMontantOperation();
                    textBox_Restant.Text = "" + op.getMontantRestant();

                   
                    membreTrouve = op.getMembre();
                    position = i;

                    button_Precedant.Visible = true;
                    break;
                }

                i++;

            }
            if (position >= nb)
                button_Precedant.Visible = false;

            if (position <= 0)
            {
                button_Suivant.Visible = false;
            }
            else
            {
                button_Suivant.Visible = true;
            }
        }

        

        private void Formulaire_ConsulteMembreOperations_Load(object sender, EventArgs e)
        {
            IFormatter format = new BinaryFormatter();
            if (System.IO.File.Exists("operationMembre.aesms"))
            {
                using (Stream flux = new FileStream("operationMembre.aesms", FileMode.Open, FileAccess.Read))
                {

                    int i = 0;
                    List<Operation> Lecture;
                    Liste_Lecture = Lecture = (List<Operation>)format.Deserialize(flux);
                    int cpt = 0;
                    foreach (Operation op in Lecture)
                    {
                        i++;
                        textBox_ID.Text = "" + op.getId();
                        textBox_Date.Text = op.getDate().Day + "/" + op.getDate().Month + "/" + op.getDate().Year;
                        string type = "";
                        switch (op.getTypeOperation())
                        {
                            case 1:
                                type = "Dépot";
                                break;
                            case 2:
                                type = "Emprunt";
                                break;
                            case 3:
                                type = "Reboursement d'emprunt";
                                break;
                            case 4:
                                type = "Frais d'un événement";
                                break;
                            case 5:
                                type = "Autres frais supplémentaires";
                                break;
                            case 0:
                                type = "Aucune opération";
                                break;
                        }
                        membreTrouve = op.getMembre();

                        textBox_TypeOperation.Text = type;
                        textBox_MontantOperation.Text = "" + op.getMontantOperation();
                        textBox_Restant.Text = "" + op.getMontantRestant();
                        cpt++;

                    }
                  
                    
                    nb = i - 1;
                    if (i > 1)
                    {
                        button_Suivant.Visible = true;
                    }
                    position = i - 1;
                }
            }
            else
            {
                MessageBox.Show("Désolé, veillez réaliser au moin une opération avant de continuer !", "Resultat Echec", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

        }
    }
}
