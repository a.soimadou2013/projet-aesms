﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AESMS
{
    public partial class Formulaire_ImpressionCarte : Form
    {
        public Formulaire_ImpressionCarte()
        {
            InitializeComponent();
        }

        static Int32 position(Int32 o)
        {
            Int32 marge = 38;
            return ((o * marge) / 100);
        }

        private void printPreviewDialog_CarteAdhesion_Load(object sender, EventArgs e)
        {


        }
        bool plein = false;
        List<Membre> liste2;
        int cptPosition = 1, cpt = 0, hauteur = 0, largeur = 0,widht=0,height=0;

        private void button_Retour_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button_Main_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        private void Formulaire_ImpressionCarte_Load(object sender, EventArgs e)
        {
            //
        }

        List<Membre> liste_Ecriture = new List<Membre>();

        private void printDocument_Verso_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Image verso = Properties.Resources.Moheli_island_map_general_view;
            cptPosition = 1;
            cpt =  hauteur =  largeur =0;

            foreach (Membre m in liste2)
            {
                if (cptPosition <= 8)
                {


                    Int32 taille = position(30);

                    e.Graphics.DrawImage(verso, (widht+19) - largeur, 0 + hauteur, widht, height);

                }
                
                cptPosition++;
                if (cpt == 1)
                {
                    cpt = -1;
                    hauteur += height + 38;

                }
                cpt++;
                if (cpt == 1)
                {
                    largeur = widht + 19;
                }
                else
                {
                    largeur = 0;
                }
            }
        }

        private void printDocument_CarteAdhesion_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            IFormatter format = new BinaryFormatter();
          
            if (System.IO.File.Exists("Liste_Carte.aesms"))
            {
                List<Membre> liste = new List<Membre>();

                using (Stream flux = new FileStream("Liste_Carte.aesms", FileMode.Open, FileAccess.Read))
                {
                    liste2=liste = (List<Membre>)format.Deserialize(flux);
                }

                Bitmap bmp = Properties.Resources.carte_MODEL21;
                Image newImage = bmp, m2 = Properties.Resources.File_Pictures_Icon_256;
                // printDocument_CarteAdhesion.DefaultPageSettings.Landscape = true;

                
                
                foreach(Membre m in liste)
                {
                    if(cptPosition<=8)
                    {
                        string dir = @"Image_Membre\" + m.getImage() + ".jpg";
                        string chemin = Path.GetFullPath(dir);
                        if (System.IO.File.Exists(chemin))
                        {
                            Image img = Image.FromFile(chemin);
                            m2 = img;

                        }

                        Int32 taille = position(30);

                        e.Graphics.DrawImage(newImage, 0 + largeur, 0 + hauteur, position(newImage.Width), position(newImage.Height));
                        if(cptPosition==1)
                        {
                            widht = position(newImage.Width);
                            height = position(newImage.Height);
                        }
                        e.Graphics.DrawImage(m2, 299 + largeur, 69 + hauteur, 82, 79);

                        e.Graphics.DrawString(m.getNom(), new Font("MV Boli", taille, FontStyle.Regular), Brushes.WhiteSmoke, new Point(position(215) + largeur, position(139) + hauteur));
                        e.Graphics.DrawString(m.getPrenom(), new Font("MV Boli", taille, FontStyle.Regular), Brushes.WhiteSmoke, new Point(position(225) + largeur, position(206) + hauteur));
                        e.Graphics.DrawString(m.getDateNaissance(), new Font("MV Boli", taille, FontStyle.Regular), Brushes.WhiteSmoke, new Point(position(310) + largeur, position(271) + hauteur));
                        e.Graphics.DrawString("Comorienne", new Font("MV Boli", taille, FontStyle.Regular), Brushes.WhiteSmoke, new Point(position(300) + largeur, position(338) + hauteur));
                        e.Graphics.DrawString(m.getFormation(), new Font("MV Boli", taille, FontStyle.Regular), Brushes.WhiteSmoke, new Point(position(300) + largeur, position(399) + hauteur));
                        e.Graphics.DrawString(m.getTelephone(), new Font("MV Boli", taille, FontStyle.Regular), Brushes.WhiteSmoke, new Point(position(240) + largeur, position(461) + hauteur));
                        e.Graphics.DrawString("Avant " + (DateTime.Now.Day+"/"+ DateTime.Now.Month+"/"+ (DateTime.Now.Year+1)), new Font("MV Boli", taille, FontStyle.Regular), Brushes.WhiteSmoke, new Point(position(220) + largeur, position(553) + hauteur));

                        e.Graphics.DrawString(m.getCode(), new Font("MV Boli", taille, FontStyle.Regular), Brushes.WhiteSmoke, new Point(position(720) + largeur, position(554) + hauteur));

                        
                    }
                    else
                    {
                        plein = true;
                        liste_Ecriture.Add(m);
                    }
                    cptPosition++;
                    if(cpt==1)
                    {
                        cpt = -1;
                        hauteur += position(newImage.Height) + 38;
                        
                    }
                    cpt++;
                    if(cpt==1)
                    {
                        largeur = position(newImage.Width) + 19;
                    }
                    else
                    {
                        largeur = 0;
                    }
                    
                }
                

            }
        }

        private void button_Print_Click(object sender, EventArgs e)
        {
            IFormatter format = new BinaryFormatter();
            printDocument_CarteAdhesion.Print();
            MessageBox.Show("Le verso des cartes a été généré, veillez l'imprimer aussi !", "Recto généré", MessageBoxButtons.OK, MessageBoxIcon.Information);
            printDocument_Verso.Print();
            if (plein)
            {
                if (System.IO.File.Exists("Liste_Carte.aesms"))
                    System.IO.File.Delete("Liste_Carte.aesms");
                using (Stream flux = new FileStream("Liste_Carte.aesms", FileMode.OpenOrCreate, FileAccess.Write))
                {
                    format.Serialize(flux, liste_Ecriture);
                }
            }
            else
            {
                if (System.IO.File.Exists("Liste_Carte.aesms"))
                    System.IO.File.Delete("Liste_Carte.aesms");
            }
        }

        private void button_PreviewPrint_Click(object sender, EventArgs e)
        {
            printPreviewDialog_CarteAdhesion.Document = printDocument_CarteAdhesion;

            printPreviewDialog_CarteAdhesion.ShowDialog();
        }
    }
}
