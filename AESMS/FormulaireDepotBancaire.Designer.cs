﻿namespace AESMS
{
    partial class FormulaireDepotBancaire
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormulaireDepotBancaire));
            this.button_Main = new System.Windows.Forms.Button();
            this.button_Retour = new System.Windows.Forms.Button();
            this.button_Deposer = new System.Windows.Forms.Button();
            this.textBox_MontantDepot = new System.Windows.Forms.TextBox();
            this.label_MontantDepot = new System.Windows.Forms.Label();
            this.label_CompteAssociation = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button_Main
            // 
            this.button_Main.BackgroundImage = global::AESMS.Properties.Resources.bouton_home_menu_principal;
            this.button_Main.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_Main.Location = new System.Drawing.Point(12, 69);
            this.button_Main.Name = "button_Main";
            this.button_Main.Size = new System.Drawing.Size(52, 51);
            this.button_Main.TabIndex = 20;
            this.button_Main.UseVisualStyleBackColor = true;
            this.button_Main.Click += new System.EventHandler(this.button_Main_Click);
            // 
            // button_Retour
            // 
            this.button_Retour.BackgroundImage = global::AESMS.Properties.Resources.Alarm_Arrow_Left_Icon_48;
            this.button_Retour.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_Retour.Location = new System.Drawing.Point(12, 12);
            this.button_Retour.Name = "button_Retour";
            this.button_Retour.Size = new System.Drawing.Size(52, 51);
            this.button_Retour.TabIndex = 19;
            this.button_Retour.UseVisualStyleBackColor = true;
            this.button_Retour.Click += new System.EventHandler(this.button_Retour_Click_1);
            // 
            // button_Deposer
            // 
            this.button_Deposer.BackColor = System.Drawing.SystemColors.Control;
            this.button_Deposer.BackgroundImage = global::AESMS.Properties.Resources.Button_Add_Icon_96;
            this.button_Deposer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button_Deposer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Deposer.Location = new System.Drawing.Point(234, 156);
            this.button_Deposer.Name = "button_Deposer";
            this.button_Deposer.Size = new System.Drawing.Size(126, 131);
            this.button_Deposer.TabIndex = 18;
            this.button_Deposer.Text = "Déposer";
            this.button_Deposer.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button_Deposer.UseVisualStyleBackColor = false;
            this.button_Deposer.Click += new System.EventHandler(this.button_Deposer_Click_1);
            // 
            // textBox_MontantDepot
            // 
            this.textBox_MontantDepot.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_MontantDepot.Location = new System.Drawing.Point(304, 94);
            this.textBox_MontantDepot.Name = "textBox_MontantDepot";
            this.textBox_MontantDepot.Size = new System.Drawing.Size(143, 22);
            this.textBox_MontantDepot.TabIndex = 17;
            // 
            // label_MontantDepot
            // 
            this.label_MontantDepot.AutoSize = true;
            this.label_MontantDepot.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_MontantDepot.Location = new System.Drawing.Point(103, 96);
            this.label_MontantDepot.Name = "label_MontantDepot";
            this.label_MontantDepot.Size = new System.Drawing.Size(156, 20);
            this.label_MontantDepot.TabIndex = 16;
            this.label_MontantDepot.Text = "Montant de dépot:";
            // 
            // label_CompteAssociation
            // 
            this.label_CompteAssociation.AutoSize = true;
            this.label_CompteAssociation.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_CompteAssociation.ForeColor = System.Drawing.Color.DarkOrange;
            this.label_CompteAssociation.Location = new System.Drawing.Point(99, 12);
            this.label_CompteAssociation.Name = "label_CompteAssociation";
            this.label_CompteAssociation.Size = new System.Drawing.Size(423, 42);
            this.label_CompteAssociation.TabIndex = 15;
            this.label_CompteAssociation.Text = "Espace dépot bancaire";
            // 
            // FormulaireDepotBancaire
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::AESMS.Properties.Resources.bg;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(611, 308);
            this.Controls.Add(this.button_Main);
            this.Controls.Add(this.button_Retour);
            this.Controls.Add(this.button_Deposer);
            this.Controls.Add(this.textBox_MontantDepot);
            this.Controls.Add(this.label_MontantDepot);
            this.Controls.Add(this.label_CompteAssociation);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormulaireDepotBancaire";
            this.Text = "Formulaire de dépot bancaire";
            this.Load += new System.EventHandler(this.FormulaireDepotBancaire_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_Main;
        private System.Windows.Forms.Button button_Retour;
        private System.Windows.Forms.Button button_Deposer;
        private System.Windows.Forms.TextBox textBox_MontantDepot;
        private System.Windows.Forms.Label label_MontantDepot;
        private System.Windows.Forms.Label label_CompteAssociation;
    }
}