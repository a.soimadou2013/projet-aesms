﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AESMS
{
    public partial class SousMenu_GestionMembre : Form
    {
        public SousMenu_GestionMembre()
        {
            InitializeComponent();
        }

        private void button_AjouterMembre_Click(object sender, EventArgs e)
        {
            Formulaire_AjoutMembre f = new Formulaire_AjoutMembre();
            this.Hide();
            f.ShowDialog();
            this.Show();
            //
        }

        private void SousMenu_GestionMembre_Load(object sender, EventArgs e)
        {
            //
        }

        private void button_RechercherMembre_Click(object sender, EventArgs e)
        {
            Formulaire_Recherche frecherche = new Formulaire_Recherche();
            this.Hide();
            frecherche.ShowDialog();
            this.Show();
            //
        }

        private void button_ModifierMembre_Click(object sender, EventArgs e)
        {
            Formulaire_ModifierMembre fmodifier = new Formulaire_ModifierMembre();
            this.Hide();
            fmodifier.ShowDialog();
            this.Show();
            //
        }

        private void button_SupprimerMembre_Click(object sender, EventArgs e)
        {
            Formulaire_SupprimerMembre fsupp = new Formulaire_SupprimerMembre();
            this.Hide();
            fsupp.ShowDialog();
            this.Show();
            //
        }

        private void button_ImprimerCarteMembre_Click(object sender, EventArgs e)
        {
            Formulaire_ImpressionCarte printCarte = new Formulaire_ImpressionCarte();
            this.Hide();
            printCarte.ShowDialog();
            this.Show();
            //
        }

        private void button_Retour_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button_Main_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }
    }
}
