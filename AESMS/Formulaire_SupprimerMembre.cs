﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AESMS
{
    public partial class Formulaire_SupprimerMembre : Form
    {
        public Formulaire_SupprimerMembre()
        {
            InitializeComponent();
        }

        private void button_Supprimer_Click(object sender, EventArgs e)
        {
            if (comboBox_ListeMembre.Text.Equals("Liste des membres de l'association AESMS") || comboBox_ListeMembre.Text.Equals("Il n'y a aucun membre ajouté dans le système !"))
            {
                MessageBox.Show("Veillez selectionner un membre avant de continuer !", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                string ch = "", str = comboBox_ListeMembre.Text;
                int cpt = 0;
                bool ok = false;
                for (int i = 0; i < str.Length; i++)
                {
                    if (str[i] == ' ' && cpt < 3)
                        cpt++;
                    if (cpt > 2)
                    {
                        if (str[i] == ' ')
                        {
                            if (str[i + 1] >= '0' && str[i + 1] <= '9')
                                break;
                        }

                        ch += str[i];
                    }
                    if (cpt == 2)
                        cpt++;

                }
                str = ch;

                cpt = 0;
                ch = "";
                string code = "";
                for (int i = 0; i < str.Length; i++)
                {
                    ch = "";
                    if (str[i] == ' ' && ok == false)
                    {
                        ch = "" + str[i] + str[i + 1] + str[i + 2] + str[i + 3] + str[i + 4] + str[i + 5];
                        if (ch.Equals(" code:"))
                        {
                            i = i + 6;
                            ok = true;
                        }
                    }

                    if (ok)
                    {
                        code += str[i];

                    }
                }
                bool trouve=false;

                if (MessageBox.Show("Voulez-vous vraiment supprimer ce membre ?","Confirmation",MessageBoxButtons.YesNo,MessageBoxIcon.Question)==DialogResult.Yes)
                {
                    if (System.IO.File.Exists("Membre.aesms"))
                    {
                        IFormatter format = new BinaryFormatter();
                        
                        List<Membre> ecriture = new List<Membre>();
                        using (Stream flux = new FileStream("Membre.aesms", FileMode.Open, FileAccess.Read))
                        {
                            List<Membre> Liste_Lecture;
                            List<Membre> Liste_Trouver = new List<Membre>();


                            Liste_Lecture = (List<Membre>)format.Deserialize(flux);
                            foreach (Membre m in Liste_Lecture)
                            {
                                
                                    if (code.Equals(m.getCode()) && m.getActive())
                                    {
                                        m.setActive(false);
                                        ecriture.Add(m);
                                    trouve = true;
                                    }
                                    else
                                    {
                                        ecriture.Add(m);
                                    }
                                
                            }
                        }
                        if(trouve)
                        {
                            if (System.IO.File.Exists("Membre.aesms"))
                            {
                                System.IO.File.Delete("Membre.aesms");
                            }
                            using (Stream flux = new FileStream("Membre.aesms", FileMode.Create, FileAccess.Write))
                            {
                                format.Serialize(flux, ecriture);
                            }
                            MessageBox.Show("Membre supprimé avec succes !", "Suppression effectué", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            comboBox_ListeMembre.Items.Clear();
                            Formulaire_SupprimerMembre_Load(sender, e);
                        }
                        else
                        {
                            MessageBox.Show("Désolé, ce membre n'existe pas !", "Membre introuvable", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        
                    }
                    else
                    {
                        MessageBox.Show("Attention, le fichier Membre.aesms est introuvable !", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                else
                {
                    MessageBox.Show("Opération annulé", "Annulé", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                
                
                
            }
        }

        private void Formulaire_SupprimerMembre_Load(object sender, EventArgs e)
        {
            IFormatter format = new BinaryFormatter();
            //

            if (System.IO.File.Exists("Membre.aesms"))
            {
                using (Stream flux = new FileStream("Membre.aesms", FileMode.Open, FileAccess.Read))
                {
                    List<Membre> Liste_Lecture;
                    Liste_Lecture = (List<Membre>)format.Deserialize(flux);
                    int i = 1;
                    foreach (Membre m in Liste_Lecture)
                    {
                        if(m.getActive())
                        {
                            comboBox_ListeMembre.Items.Add("" + i + " - " + m.getNom() + " " + m.getPrenom() + " code:" + m.getCode());
                            i++;
                        }
                        
                    }
                }

            }
            else
            {
                comboBox_ListeMembre.Items.Add("Il n'y a aucun membre ajouté dans le système !");
            }
            if (System.IO.File.Exists("MembreModifier.aesms"))
            {
                System.IO.File.Delete("MembreModifier.aesms");
            }
        }

        private void comboBox_ListeMembre_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button_Retour_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button_Main_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }
    }
}
