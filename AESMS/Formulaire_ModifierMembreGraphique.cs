﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AESMS
{
    public partial class Formulaire_ModifierMembreGraphique : Form
    {
        public Formulaire_ModifierMembreGraphique()
        {
            InitializeComponent();
        }

        Membre m;
        bool choisirImage = false;
        private int nombreDeClicPivoter=1;

        private void Formulaire_ModifierMembreGraphique_Load(object sender, EventArgs e)
        {
            //
            if (System.IO.File.Exists("MembreModifier.aesms"))
            {
                IFormatter format = new BinaryFormatter();
                using (Stream flux = new FileStream("MembreModifier.aesms", FileMode.Open, FileAccess.Read))
                {


                    m = (Membre)format.Deserialize(flux);
                    if(m.getActive())
                    {
                        textBox_Nom.Text = m.getNom();
                        textBox_Prenom.Text = m.getPrenom();
                        textBox_DateNaissance.Text = m.getDateNaissance();
                        textBox_Sexe.Text = m.getSexe();
                        textBox_Formation.Text = m.getFormation();
                        textBox_Adresse.Text = m.getAdresse();
                        textBox_Telephone.Text = m.getTelephone();
                        /*
                        string dir = @"Image_Membre\" + m.getImage() + ".jpg";
                        string chemin = Path.GetFullPath(dir);
                        if (System.IO.File.Exists(chemin))
                        {
                            Image img = Image.FromFile(chemin);
                            pictureBox_Photo.Image = img;

                        }
                        else
                        {
                            pictureBox_Photo.Image = null;
                        }
                        */

                    }

                }
            }
            else
            {
            }
        }

        private void button_Sauvegarder_Click(object sender, EventArgs e)
        {
            List<Membre>ecriture = new List<Membre>();
            if(m.getActive())
            {
                m.setNom(textBox_Nom.Text);
                m.setPrenom(textBox_Prenom.Text);
                m.setDateNaissance(textBox_DateNaissance.Text);
                m.setSexe(textBox_Sexe.Text);
                m.setFormation(textBox_Formation.Text);
                m.setAdresse(textBox_Adresse.Text);
                m.setTelephone(textBox_Telephone.Text);
                if (choisirImage)
                {
                    m.setImage(m.getCode());

                    System.IO.Directory.CreateDirectory("Image_Membre");
                    string adresse = "";
                    if (choisirImage)
                    {
                        
                        string image = @"Image_Membre\" + m.getCode();
                        string source = pictureBox_Photo.ImageLocation;
                        
                        adresse = image + ".jpg";
                        if (System.IO.File.Exists(adresse))
                            System.IO.File.Delete(adresse);
                        pictureBox_Photo.Image.Save(adresse);
                    }
                }



                if (System.IO.File.Exists("Membre.aesms"))
                {
                    IFormatter format = new BinaryFormatter();
                    using (Stream flux = new FileStream("Membre.aesms", FileMode.Open, FileAccess.ReadWrite))
                    {

                        List<Membre> lecture;

                        lecture = (List<Membre>)format.Deserialize(flux);



                        foreach (Membre m2 in lecture)
                        {
                            if (m2.getCode().Equals(m.getCode()) && m2.getActive())
                            {
                                ecriture.Add(m);
                            }
                            else
                            {
                                ecriture.Add(m2);
                            }
                        }

                    }
                    if (System.IO.File.Exists("Membre.aesms"))
                    {
                        System.IO.File.Delete("Membre.aesms");
                    }
                    if (System.IO.File.Exists("MembreModifier.aesms"))
                    {
                        System.IO.File.Delete("MembreModifier.aesms");
                    }
                    using (Stream flux = new FileStream("Membre.aesms", FileMode.Create, FileAccess.Write))
                    {
                        format.Serialize(flux, ecriture);
                    }
                    MessageBox.Show("Modification effectué avec succès !", "Modification éffectué", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            
        }

        private void pictureBox_Photo_Click(object sender, EventArgs e)
        {
            
            OpenFileDialog open = new OpenFileDialog();
            //open.Filter = "Bitmaps|*.bmp|jpeps|*.jpg";

            if (open.ShowDialog() == DialogResult.OK)
            {
                choisirImage = true;
                pictureBox_Photo.ImageLocation = open.FileName;
            }
        }

        private void button_Pivoter_Click(object sender, EventArgs e)
        {
            Image img = pictureBox_Photo.Image;

            switch (nombreDeClicPivoter)
            {
                case 1:
                    img.RotateFlip(RotateFlipType.Rotate90FlipNone);
                    break;
                case 2:
                    img.RotateFlip(RotateFlipType.Rotate180FlipNone);
                    break;
                case 3:
                    img.RotateFlip(RotateFlipType.Rotate270FlipNone);
                    nombreDeClicPivoter = 1;
                    break;


            }
            nombreDeClicPivoter++;
            pictureBox_Photo.Image = img;
        }

        private void button_Retour_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button_Main_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }
    }
}
