﻿namespace AESMS
{
    partial class Formulaire_Recherche
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Formulaire_Recherche));
            this.textBox_Recherche = new System.Windows.Forms.TextBox();
            this.button_Rechercher = new System.Windows.Forms.Button();
            this.comboBox_ListeMembre = new System.Windows.Forms.ComboBox();
            this.button_Retour = new System.Windows.Forms.Button();
            this.button_Main = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox_Recherche
            // 
            this.textBox_Recherche.Location = new System.Drawing.Point(46, 74);
            this.textBox_Recherche.Name = "textBox_Recherche";
            this.textBox_Recherche.Size = new System.Drawing.Size(177, 20);
            this.textBox_Recherche.TabIndex = 0;
            // 
            // button_Rechercher
            // 
            this.button_Rechercher.Location = new System.Drawing.Point(81, 100);
            this.button_Rechercher.Name = "button_Rechercher";
            this.button_Rechercher.Size = new System.Drawing.Size(101, 30);
            this.button_Rechercher.TabIndex = 1;
            this.button_Rechercher.Text = "Rechercher";
            this.button_Rechercher.UseVisualStyleBackColor = true;
            this.button_Rechercher.Click += new System.EventHandler(this.button_Rechercher_Click);
            // 
            // comboBox_ListeMembre
            // 
            this.comboBox_ListeMembre.FormattingEnabled = true;
            this.comboBox_ListeMembre.Location = new System.Drawing.Point(254, 74);
            this.comboBox_ListeMembre.Name = "comboBox_ListeMembre";
            this.comboBox_ListeMembre.Size = new System.Drawing.Size(211, 21);
            this.comboBox_ListeMembre.TabIndex = 2;
            this.comboBox_ListeMembre.Text = "Liste des membres";
            this.comboBox_ListeMembre.SelectedIndexChanged += new System.EventHandler(this.comboBox_ListeMembre_SelectedIndexChanged);
            // 
            // button_Retour
            // 
            this.button_Retour.BackgroundImage = global::AESMS.Properties.Resources.Alarm_Arrow_Left_Icon_48;
            this.button_Retour.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_Retour.Location = new System.Drawing.Point(12, 12);
            this.button_Retour.Name = "button_Retour";
            this.button_Retour.Size = new System.Drawing.Size(52, 51);
            this.button_Retour.TabIndex = 6;
            this.button_Retour.UseVisualStyleBackColor = true;
            this.button_Retour.Click += new System.EventHandler(this.button_Retour_Click);
            // 
            // button_Main
            // 
            this.button_Main.BackgroundImage = global::AESMS.Properties.Resources.bouton_home_menu_principal;
            this.button_Main.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_Main.Location = new System.Drawing.Point(70, 12);
            this.button_Main.Name = "button_Main";
            this.button_Main.Size = new System.Drawing.Size(52, 51);
            this.button_Main.TabIndex = 8;
            this.button_Main.UseVisualStyleBackColor = true;
            this.button_Main.Click += new System.EventHandler(this.button_Main_Click);
            // 
            // Formulaire_Recherche
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::AESMS.Properties.Resources.bg;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(477, 277);
            this.Controls.Add(this.button_Main);
            this.Controls.Add(this.button_Retour);
            this.Controls.Add(this.comboBox_ListeMembre);
            this.Controls.Add(this.button_Rechercher);
            this.Controls.Add(this.textBox_Recherche);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Formulaire_Recherche";
            this.Text = "Formulaire de recherche";
            this.Load += new System.EventHandler(this.Formulaire_Recherche_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_Recherche;
        private System.Windows.Forms.Button button_Rechercher;
        private System.Windows.Forms.ComboBox comboBox_ListeMembre;
        private System.Windows.Forms.Button button_Retour;
        private System.Windows.Forms.Button button_Main;
    }
}