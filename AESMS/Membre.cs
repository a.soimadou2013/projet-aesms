﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AESMS
{
    [Serializable]
    class Membre
    {
        private string nom;
        private string prenom;
        private string dateNaissance;
        int age;
        private string sexe;
        private string formation;
        private string adresse;
        private string telephone;
        private string code;
        private string image;
        private bool active;
        private List<Operation> operationMembre = new List<Operation>();
        


        public Membre()
        {
            nom = "";
            prenom = "";
            dateNaissance = "";
            age = 0;
            sexe = "";
            formation = "";
            adresse = "";
            telephone = "";
            code = "";
            image = "";
            active = true;
        }

        public List<Operation> getOperationMembre()
        {
            return operationMembre;
        }
        public bool getActive()
        {
            return active;
        }
        public string getImage()
        {
            return image;
        }
        public string getCode()
        {
            return code;
        }
        public string getNom()
        {
            return nom;
        }
        public string getPrenom()
        {
            return prenom;
        }
        public string getDateNaissance()
        {
            return dateNaissance;
        }
        public int getAge()
        {
            return age;
        }
        public string getSexe()
        {
            return sexe;
        }
        public string getFormation()
        {
            return formation;
        }
        public string getAdresse()
        {
            return adresse;
        }
        public string getTelephone()
        {
            return telephone;
        }


        public void setCode(string code)
        {
            this.code = code;
        }
        public void setNom(string nom)
        {
            this.nom = nom;
        }
        public void setPrenom(string prenom)
        {
            this.prenom = prenom;
        }
        public void setDateNaissance(string dateNaissance)
        {
            this.dateNaissance = dateNaissance;
            string ch = "";
            int cpt = 0;
            for(int i=0;i<this.dateNaissance.Length;i++)
            {
                if (this.dateNaissance[i] == '/')
                    cpt++;
               
                if(cpt>2)
                {
                    ch += this.dateNaissance[i];
                }
                if (cpt == 2)
                    cpt++;
            }
            
            age = DateTime.Now.Year - Convert.ToInt32(ch);
        }

        public void setSexe(string sexe)
        {
            this.sexe = sexe;
        }
        public void setFormation(string formation)
        {
            this.formation = formation;
        }
        public void setAdresse(string adresse)
        {
            this.adresse = adresse;
        }
        public void setTelephone(string telephone)
        {
            this.telephone = telephone;
        }

        public void setImage(string image)
        {
            this.image = image;
        }
        public void setActive(bool active)
        {
            this.active = active;
        }
        public void setOperationMembre(List<Operation> operationMembre)
        {
            this.operationMembre = operationMembre;
        }


    }
}
