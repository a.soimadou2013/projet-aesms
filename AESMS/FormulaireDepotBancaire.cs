﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AESMS
{
    public partial class FormulaireDepotBancaire : Form
    {
        public FormulaireDepotBancaire()
        {
            InitializeComponent();
        }

        

        private void button_Retour_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button_Main_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        private void FormulaireDepotBancaire_Load(object sender, EventArgs e)
        {
            textBox_MontantDepot.Text = "";
        }

        private void button_Deposer_Click_1(object sender, EventArgs e)
        {
            Operation op = new Operation();
            CompteBancaire compte = new CompteBancaire();
            List<Operation> liste_Op = new List<Operation>();
            List<Operation> temp = new List<Operation>();
            long cptop = 1;
            IFormatter format = new BinaryFormatter();
            string str = textBox_MontantDepot.Text;
            int m = 0;
            int.TryParse(str, out m);
            if (!m.Equals(0))
            {
                if (System.IO.File.Exists("operation.aesms"))
                {
                    using (Stream flux = new FileStream("operation.aesms", FileMode.Open, FileAccess.Read))
                    {
                        temp = (List<Operation>)format.Deserialize(flux);
                    }
                    liste_Op = temp;
                    cptop = 0;
                    foreach (Operation o in temp)
                    {
                        cptop++;
                    }
                    cptop++;
                    op.setId(cptop);
                    op.setTypeOperation(1);
                    DateTime date = DateTime.Now.Date;
                    op.setDate(date);

                    string ch = "";
                    ch = textBox_MontantDepot.Text;
                    int montantDepot = int.Parse(ch);
                    op.setMontantOperation(montantDepot);
                    if (System.IO.File.Exists("ressources.aesms"))
                    {
                        using (Stream flux = new FileStream("ressources.aesms", FileMode.Open, FileAccess.Read))
                        {
                            compte = (CompteBancaire)format.Deserialize(flux);
                        }
                    }
                    op.setMontantRestant(compte.getMontant() + montantDepot);
                    compte.setMontant(op.getMontantRestant());

                    liste_Op.Add(op);
                }
                else
                {
                    op.setId(cptop);
                    op.setTypeOperation(1);
                    DateTime date = DateTime.Now;
                    op.setDate(date);

                    string ch = "";
                    ch = textBox_MontantDepot.Text;
                    int montantDepot = int.Parse(ch);
                    op.setMontantOperation(montantDepot);
                    if (System.IO.File.Exists("ressources.aesms"))
                    {
                        using (Stream flux = new FileStream("ressources.aesms", FileMode.Open, FileAccess.Read))
                        {
                            compte = (CompteBancaire)format.Deserialize(flux);
                        }
                    }
                    op.setMontantRestant(compte.getMontant() + montantDepot);
                    compte.setMontant(op.getMontantRestant());

                    liste_Op.Add(op);
                }

                if (System.IO.File.Exists("ressources.aesms"))
                    System.IO.File.Delete("ressources.aesms");

                using (Stream flux = new FileStream("ressources.aesms", FileMode.Create, FileAccess.Write))
                {
                    format.Serialize(flux, compte);
                }

                if (System.IO.File.Exists("operation.aesms"))
                    System.IO.File.Delete("operation.aesms");
                using (Stream flux = new FileStream("operation.aesms", FileMode.Create, FileAccess.Write))
                {
                    format.Serialize(flux, liste_Op);
                }
                MessageBox.Show("Dépot éffectué avec succès !", "Dépot éffectué", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            else
            {
                MessageBox.Show("Désolé, ce montant est insuffisant , veillez saisir un montant supérieur a 0 !", "Echec de dépot", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                textBox_MontantDepot.Text = "";
                textBox_MontantDepot.Focus();
            }
            textBox_MontantDepot.Text = "";

        }

       
    }
}
