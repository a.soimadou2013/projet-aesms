﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AESMS
{
    [Serializable]
    class CompteBancaire
    {
        private string Organisme;
        private bool statue;
        private int montant;
       

        public CompteBancaire()
        {
            statue = true;
            montant = 0;
            Organisme = "COMPTE BANCAIRE AESMS";
        }
        public string getOrganisme()
        {
            return Organisme;
        }
        public bool getStatue()
        {
            return statue;
        }
        public int getMontant()
        {
            return montant;
        }

        public void setOrganisme(string Organisme)
        {
            this.Organisme = Organisme;
        }

        public void setStatue(bool statue)
        {
            this.statue=statue;
        }

        public void setMontant(int montant)
        {
            this.montant=montant;
        }
    }
}
