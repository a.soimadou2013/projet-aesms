﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AESMS
{
    public partial class Formulaire_SoldeBancaire : Form
    {
        public Formulaire_SoldeBancaire()
        {
            InitializeComponent();
        }

        private void Formulaire_SoldeBancaire_Load(object sender, EventArgs e)
        {
            CompteBancaire compte = new CompteBancaire();
            IFormatter format = new BinaryFormatter();

            if (System.IO.File.Exists("ressources.aesms"))
            {
                using (Stream flux = new FileStream("ressources.aesms", FileMode.Open, FileAccess.Read))
                {
                    compte = (CompteBancaire)format.Deserialize(flux);
                }
            }
            else
            {
                using (Stream flux = new FileStream("ressources.aesms", FileMode.Create, FileAccess.Write))
                {
                    format.Serialize(flux, compte);
                }
            }
            textBox_MontantSolde.Text = ""+compte.getMontant();
        }

        private void button_Retour_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button_Main_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }
    }
}
