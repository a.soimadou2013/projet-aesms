﻿namespace AESMS
{
    partial class Formulaire_ModifierMembreGraphique
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Formulaire_ModifierMembreGraphique));
            this.textBox_Sexe = new System.Windows.Forms.TextBox();
            this.textBox_DateNaissance = new System.Windows.Forms.TextBox();
            this.label_Sexe = new System.Windows.Forms.Label();
            this.textBox_Telephone = new System.Windows.Forms.TextBox();
            this.label_Telephone = new System.Windows.Forms.Label();
            this.textBox_Adresse = new System.Windows.Forms.TextBox();
            this.textBox_Formation = new System.Windows.Forms.TextBox();
            this.label_Formation = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label_DateNaissance = new System.Windows.Forms.Label();
            this.textBox_Prenom = new System.Windows.Forms.TextBox();
            this.label_Prenom = new System.Windows.Forms.Label();
            this.textBox_Nom = new System.Windows.Forms.TextBox();
            this.label_Nom = new System.Windows.Forms.Label();
            this.button_Sauvegarder = new System.Windows.Forms.Button();
            this.pictureBox_Photo = new System.Windows.Forms.PictureBox();
            this.button_Pivoter = new System.Windows.Forms.Button();
            this.button_Retour = new System.Windows.Forms.Button();
            this.button_Main = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Photo)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox_Sexe
            // 
            this.textBox_Sexe.Location = new System.Drawing.Point(274, 208);
            this.textBox_Sexe.Name = "textBox_Sexe";
            this.textBox_Sexe.Size = new System.Drawing.Size(122, 20);
            this.textBox_Sexe.TabIndex = 73;
            // 
            // textBox_DateNaissance
            // 
            this.textBox_DateNaissance.Location = new System.Drawing.Point(243, 162);
            this.textBox_DateNaissance.Name = "textBox_DateNaissance";
            this.textBox_DateNaissance.Size = new System.Drawing.Size(153, 20);
            this.textBox_DateNaissance.TabIndex = 72;
            // 
            // label_Sexe
            // 
            this.label_Sexe.AutoSize = true;
            this.label_Sexe.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label_Sexe.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label_Sexe.Location = new System.Drawing.Point(61, 208);
            this.label_Sexe.Name = "label_Sexe";
            this.label_Sexe.Size = new System.Drawing.Size(49, 20);
            this.label_Sexe.TabIndex = 70;
            this.label_Sexe.Text = "Sexe:";
            // 
            // textBox_Telephone
            // 
            this.textBox_Telephone.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBox_Telephone.Location = new System.Drawing.Point(243, 355);
            this.textBox_Telephone.Name = "textBox_Telephone";
            this.textBox_Telephone.Size = new System.Drawing.Size(153, 26);
            this.textBox_Telephone.TabIndex = 69;
            // 
            // label_Telephone
            // 
            this.label_Telephone.AutoSize = true;
            this.label_Telephone.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label_Telephone.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label_Telephone.Location = new System.Drawing.Point(61, 358);
            this.label_Telephone.Name = "label_Telephone";
            this.label_Telephone.Size = new System.Drawing.Size(112, 20);
            this.label_Telephone.TabIndex = 68;
            this.label_Telephone.Text = "No Téléphone:";
            // 
            // textBox_Adresse
            // 
            this.textBox_Adresse.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBox_Adresse.Location = new System.Drawing.Point(196, 304);
            this.textBox_Adresse.Name = "textBox_Adresse";
            this.textBox_Adresse.Size = new System.Drawing.Size(200, 26);
            this.textBox_Adresse.TabIndex = 67;
            // 
            // textBox_Formation
            // 
            this.textBox_Formation.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBox_Formation.Location = new System.Drawing.Point(220, 256);
            this.textBox_Formation.Name = "textBox_Formation";
            this.textBox_Formation.Size = new System.Drawing.Size(176, 26);
            this.textBox_Formation.TabIndex = 65;
            // 
            // label_Formation
            // 
            this.label_Formation.AutoSize = true;
            this.label_Formation.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label_Formation.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label_Formation.Location = new System.Drawing.Point(61, 259);
            this.label_Formation.Name = "label_Formation";
            this.label_Formation.Size = new System.Drawing.Size(85, 20);
            this.label_Formation.TabIndex = 64;
            this.label_Formation.Text = "Formation:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label6.Location = new System.Drawing.Point(61, 307);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 20);
            this.label6.TabIndex = 66;
            this.label6.Text = "Adresse:";
            // 
            // label_DateNaissance
            // 
            this.label_DateNaissance.AutoSize = true;
            this.label_DateNaissance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label_DateNaissance.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label_DateNaissance.Location = new System.Drawing.Point(61, 162);
            this.label_DateNaissance.Name = "label_DateNaissance";
            this.label_DateNaissance.Size = new System.Drawing.Size(146, 20);
            this.label_DateNaissance.TabIndex = 63;
            this.label_DateNaissance.Text = "Date de naissance:";
            // 
            // textBox_Prenom
            // 
            this.textBox_Prenom.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBox_Prenom.Location = new System.Drawing.Point(220, 108);
            this.textBox_Prenom.Name = "textBox_Prenom";
            this.textBox_Prenom.Size = new System.Drawing.Size(176, 26);
            this.textBox_Prenom.TabIndex = 62;
            // 
            // label_Prenom
            // 
            this.label_Prenom.AutoSize = true;
            this.label_Prenom.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label_Prenom.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label_Prenom.Location = new System.Drawing.Point(61, 114);
            this.label_Prenom.Name = "label_Prenom";
            this.label_Prenom.Size = new System.Drawing.Size(68, 20);
            this.label_Prenom.TabIndex = 61;
            this.label_Prenom.Text = "Prénom:";
            // 
            // textBox_Nom
            // 
            this.textBox_Nom.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBox_Nom.Location = new System.Drawing.Point(220, 59);
            this.textBox_Nom.Name = "textBox_Nom";
            this.textBox_Nom.Size = new System.Drawing.Size(176, 26);
            this.textBox_Nom.TabIndex = 60;
            // 
            // label_Nom
            // 
            this.label_Nom.AutoSize = true;
            this.label_Nom.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label_Nom.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label_Nom.Location = new System.Drawing.Point(61, 62);
            this.label_Nom.Name = "label_Nom";
            this.label_Nom.Size = new System.Drawing.Size(46, 20);
            this.label_Nom.TabIndex = 59;
            this.label_Nom.Text = "Nom:";
            // 
            // button_Sauvegarder
            // 
            this.button_Sauvegarder.BackgroundImage = global::AESMS.Properties.Resources.Document_Add_Icon_256;
            this.button_Sauvegarder.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_Sauvegarder.Location = new System.Drawing.Point(577, 290);
            this.button_Sauvegarder.Name = "button_Sauvegarder";
            this.button_Sauvegarder.Size = new System.Drawing.Size(135, 137);
            this.button_Sauvegarder.TabIndex = 74;
            this.button_Sauvegarder.UseVisualStyleBackColor = true;
            this.button_Sauvegarder.Click += new System.EventHandler(this.button_Sauvegarder_Click);
            // 
            // pictureBox_Photo
            // 
            this.pictureBox_Photo.BackgroundImage = global::AESMS.Properties.Resources.File_Pictures_Icon_256;
            this.pictureBox_Photo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox_Photo.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pictureBox_Photo.Location = new System.Drawing.Point(502, 49);
            this.pictureBox_Photo.Name = "pictureBox_Photo";
            this.pictureBox_Photo.Size = new System.Drawing.Size(210, 166);
            this.pictureBox_Photo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_Photo.TabIndex = 71;
            this.pictureBox_Photo.TabStop = false;
            this.pictureBox_Photo.Click += new System.EventHandler(this.pictureBox_Photo_Click);
            // 
            // button_Pivoter
            // 
            this.button_Pivoter.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button_Pivoter.Location = new System.Drawing.Point(577, 208);
            this.button_Pivoter.Name = "button_Pivoter";
            this.button_Pivoter.Size = new System.Drawing.Size(75, 23);
            this.button_Pivoter.TabIndex = 75;
            this.button_Pivoter.Text = "Pivoter";
            this.button_Pivoter.UseVisualStyleBackColor = true;
            this.button_Pivoter.Click += new System.EventHandler(this.button_Pivoter_Click);
            // 
            // button_Retour
            // 
            this.button_Retour.BackgroundImage = global::AESMS.Properties.Resources.Alarm_Arrow_Left_Icon_48;
            this.button_Retour.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_Retour.Location = new System.Drawing.Point(12, 12);
            this.button_Retour.Name = "button_Retour";
            this.button_Retour.Size = new System.Drawing.Size(52, 51);
            this.button_Retour.TabIndex = 76;
            this.button_Retour.UseVisualStyleBackColor = true;
            this.button_Retour.Click += new System.EventHandler(this.button_Retour_Click);
            // 
            // button_Main
            // 
            this.button_Main.BackgroundImage = global::AESMS.Properties.Resources.bouton_home_menu_principal;
            this.button_Main.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_Main.Location = new System.Drawing.Point(12, 69);
            this.button_Main.Name = "button_Main";
            this.button_Main.Size = new System.Drawing.Size(52, 51);
            this.button_Main.TabIndex = 77;
            this.button_Main.UseVisualStyleBackColor = true;
            this.button_Main.Click += new System.EventHandler(this.button_Main_Click);
            // 
            // Formulaire_ModifierMembreGraphique
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::AESMS.Properties.Resources.bg;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(773, 469);
            this.Controls.Add(this.button_Main);
            this.Controls.Add(this.button_Retour);
            this.Controls.Add(this.button_Pivoter);
            this.Controls.Add(this.button_Sauvegarder);
            this.Controls.Add(this.textBox_Sexe);
            this.Controls.Add(this.textBox_DateNaissance);
            this.Controls.Add(this.pictureBox_Photo);
            this.Controls.Add(this.label_Sexe);
            this.Controls.Add(this.textBox_Telephone);
            this.Controls.Add(this.label_Telephone);
            this.Controls.Add(this.textBox_Adresse);
            this.Controls.Add(this.textBox_Formation);
            this.Controls.Add(this.label_Formation);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label_DateNaissance);
            this.Controls.Add(this.textBox_Prenom);
            this.Controls.Add(this.label_Prenom);
            this.Controls.Add(this.textBox_Nom);
            this.Controls.Add(this.label_Nom);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Formulaire_ModifierMembreGraphique";
            this.Text = "Formulaire de modification de membre";
            this.Load += new System.EventHandler(this.Formulaire_ModifierMembreGraphique_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Photo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_Sexe;
        private System.Windows.Forms.TextBox textBox_DateNaissance;
        private System.Windows.Forms.PictureBox pictureBox_Photo;
        private System.Windows.Forms.Label label_Sexe;
        private System.Windows.Forms.TextBox textBox_Telephone;
        private System.Windows.Forms.Label label_Telephone;
        private System.Windows.Forms.TextBox textBox_Adresse;
        private System.Windows.Forms.TextBox textBox_Formation;
        private System.Windows.Forms.Label label_Formation;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label_DateNaissance;
        private System.Windows.Forms.TextBox textBox_Prenom;
        private System.Windows.Forms.Label label_Prenom;
        private System.Windows.Forms.TextBox textBox_Nom;
        private System.Windows.Forms.Label label_Nom;
        private System.Windows.Forms.Button button_Sauvegarder;
        private System.Windows.Forms.Button button_Pivoter;
        private System.Windows.Forms.Button button_Retour;
        private System.Windows.Forms.Button button_Main;
    }
}