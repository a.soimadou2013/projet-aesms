﻿namespace AESMS
{
    partial class Formulaire_SoldeBancaire
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Formulaire_SoldeBancaire));
            this.label_CompteAssociation = new System.Windows.Forms.Label();
            this.label_MontantSolde = new System.Windows.Forms.Label();
            this.textBox_MontantSolde = new System.Windows.Forms.TextBox();
            this.button_Main = new System.Windows.Forms.Button();
            this.button_Retour = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label_CompteAssociation
            // 
            this.label_CompteAssociation.AutoSize = true;
            this.label_CompteAssociation.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_CompteAssociation.ForeColor = System.Drawing.Color.DarkOrange;
            this.label_CompteAssociation.Location = new System.Drawing.Point(102, 12);
            this.label_CompteAssociation.Name = "label_CompteAssociation";
            this.label_CompteAssociation.Size = new System.Drawing.Size(531, 25);
            this.label_CompteAssociation.TabIndex = 0;
            this.label_CompteAssociation.Text = "SOLDE DU COMPTE DE L\'ASSOCIATION AESMS";
            // 
            // label_MontantSolde
            // 
            this.label_MontantSolde.AutoSize = true;
            this.label_MontantSolde.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_MontantSolde.Location = new System.Drawing.Point(103, 114);
            this.label_MontantSolde.Name = "label_MontantSolde";
            this.label_MontantSolde.Size = new System.Drawing.Size(153, 20);
            this.label_MontantSolde.TabIndex = 1;
            this.label_MontantSolde.Text = "Montant du solde:";
            // 
            // textBox_MontantSolde
            // 
            this.textBox_MontantSolde.Enabled = false;
            this.textBox_MontantSolde.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_MontantSolde.Location = new System.Drawing.Point(320, 114);
            this.textBox_MontantSolde.Name = "textBox_MontantSolde";
            this.textBox_MontantSolde.Size = new System.Drawing.Size(154, 26);
            this.textBox_MontantSolde.TabIndex = 2;
            // 
            // button_Main
            // 
            this.button_Main.BackgroundImage = global::AESMS.Properties.Resources.bouton_home_menu_principal;
            this.button_Main.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_Main.Location = new System.Drawing.Point(12, 69);
            this.button_Main.Name = "button_Main";
            this.button_Main.Size = new System.Drawing.Size(52, 51);
            this.button_Main.TabIndex = 8;
            this.button_Main.UseVisualStyleBackColor = true;
            this.button_Main.Click += new System.EventHandler(this.button_Main_Click);
            // 
            // button_Retour
            // 
            this.button_Retour.BackgroundImage = global::AESMS.Properties.Resources.Alarm_Arrow_Left_Icon_48;
            this.button_Retour.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_Retour.Location = new System.Drawing.Point(12, 12);
            this.button_Retour.Name = "button_Retour";
            this.button_Retour.Size = new System.Drawing.Size(52, 51);
            this.button_Retour.TabIndex = 7;
            this.button_Retour.UseVisualStyleBackColor = true;
            this.button_Retour.Click += new System.EventHandler(this.button_Retour_Click);
            // 
            // Formulaire_SoldeBancaire
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::AESMS.Properties.Resources.bg;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(692, 261);
            this.Controls.Add(this.button_Main);
            this.Controls.Add(this.button_Retour);
            this.Controls.Add(this.textBox_MontantSolde);
            this.Controls.Add(this.label_MontantSolde);
            this.Controls.Add(this.label_CompteAssociation);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Formulaire_SoldeBancaire";
            this.Text = "Consultation du solde bancaire";
            this.Load += new System.EventHandler(this.Formulaire_SoldeBancaire_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_CompteAssociation;
        private System.Windows.Forms.Label label_MontantSolde;
        private System.Windows.Forms.TextBox textBox_MontantSolde;
        private System.Windows.Forms.Button button_Main;
        private System.Windows.Forms.Button button_Retour;
    }
}