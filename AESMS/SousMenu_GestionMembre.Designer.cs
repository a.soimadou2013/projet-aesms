﻿namespace AESMS
{
    partial class SousMenu_GestionMembre
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SousMenu_GestionMembre));
            this.button_AjouterMembre = new System.Windows.Forms.Button();
            this.button_RechercherMembre = new System.Windows.Forms.Button();
            this.button_ModifierMembre = new System.Windows.Forms.Button();
            this.button_SupprimerMembre = new System.Windows.Forms.Button();
            this.button_ImprimerCarteMembre = new System.Windows.Forms.Button();
            this.button_Retour = new System.Windows.Forms.Button();
            this.button_Main = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button_AjouterMembre
            // 
            this.button_AjouterMembre.BackgroundImage = global::AESMS.Properties.Resources.Button_Add_Icon_96;
            this.button_AjouterMembre.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button_AjouterMembre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_AjouterMembre.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button_AjouterMembre.Location = new System.Drawing.Point(102, 30);
            this.button_AjouterMembre.Name = "button_AjouterMembre";
            this.button_AjouterMembre.Size = new System.Drawing.Size(203, 130);
            this.button_AjouterMembre.TabIndex = 0;
            this.button_AjouterMembre.Text = "Ajouter un membre";
            this.button_AjouterMembre.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button_AjouterMembre.UseVisualStyleBackColor = true;
            this.button_AjouterMembre.Click += new System.EventHandler(this.button_AjouterMembre_Click);
            // 
            // button_RechercherMembre
            // 
            this.button_RechercherMembre.BackgroundImage = global::AESMS.Properties.Resources.Search_Icon_96;
            this.button_RechercherMembre.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button_RechercherMembre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_RechercherMembre.Location = new System.Drawing.Point(445, 30);
            this.button_RechercherMembre.Name = "button_RechercherMembre";
            this.button_RechercherMembre.Size = new System.Drawing.Size(203, 130);
            this.button_RechercherMembre.TabIndex = 1;
            this.button_RechercherMembre.Text = "Recherche de membre";
            this.button_RechercherMembre.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button_RechercherMembre.UseVisualStyleBackColor = true;
            this.button_RechercherMembre.Click += new System.EventHandler(this.button_RechercherMembre_Click);
            // 
            // button_ModifierMembre
            // 
            this.button_ModifierMembre.BackgroundImage = global::AESMS.Properties.Resources.Summer_group_Icon_96;
            this.button_ModifierMembre.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_ModifierMembre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_ModifierMembre.Location = new System.Drawing.Point(102, 362);
            this.button_ModifierMembre.Name = "button_ModifierMembre";
            this.button_ModifierMembre.Size = new System.Drawing.Size(203, 146);
            this.button_ModifierMembre.TabIndex = 2;
            this.button_ModifierMembre.Text = "Modifier un membre";
            this.button_ModifierMembre.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button_ModifierMembre.UseVisualStyleBackColor = true;
            this.button_ModifierMembre.Click += new System.EventHandler(this.button_ModifierMembre_Click);
            // 
            // button_SupprimerMembre
            // 
            this.button_SupprimerMembre.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button_SupprimerMembre.BackgroundImage = global::AESMS.Properties.Resources.Remove_Male_User_Icon_96;
            this.button_SupprimerMembre.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button_SupprimerMembre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_SupprimerMembre.Location = new System.Drawing.Point(274, 211);
            this.button_SupprimerMembre.Name = "button_SupprimerMembre";
            this.button_SupprimerMembre.Size = new System.Drawing.Size(203, 104);
            this.button_SupprimerMembre.TabIndex = 3;
            this.button_SupprimerMembre.Text = "Supprimer un membre";
            this.button_SupprimerMembre.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button_SupprimerMembre.UseVisualStyleBackColor = false;
            this.button_SupprimerMembre.Click += new System.EventHandler(this.button_SupprimerMembre_Click);
            // 
            // button_ImprimerCarteMembre
            // 
            this.button_ImprimerCarteMembre.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button_ImprimerCarteMembre.BackgroundImage = global::AESMS.Properties.Resources.Hardware_Printer_Blue_Icon_96;
            this.button_ImprimerCarteMembre.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button_ImprimerCarteMembre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_ImprimerCarteMembre.Location = new System.Drawing.Point(445, 362);
            this.button_ImprimerCarteMembre.Name = "button_ImprimerCarteMembre";
            this.button_ImprimerCarteMembre.Size = new System.Drawing.Size(203, 146);
            this.button_ImprimerCarteMembre.TabIndex = 4;
            this.button_ImprimerCarteMembre.Text = "Imprimer les cartes membres";
            this.button_ImprimerCarteMembre.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button_ImprimerCarteMembre.UseVisualStyleBackColor = false;
            this.button_ImprimerCarteMembre.Click += new System.EventHandler(this.button_ImprimerCarteMembre_Click);
            // 
            // button_Retour
            // 
            this.button_Retour.BackgroundImage = global::AESMS.Properties.Resources.Alarm_Arrow_Left_Icon_48;
            this.button_Retour.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_Retour.Location = new System.Drawing.Point(12, 12);
            this.button_Retour.Name = "button_Retour";
            this.button_Retour.Size = new System.Drawing.Size(52, 51);
            this.button_Retour.TabIndex = 5;
            this.button_Retour.UseVisualStyleBackColor = true;
            this.button_Retour.Click += new System.EventHandler(this.button_Retour_Click);
            // 
            // button_Main
            // 
            this.button_Main.BackgroundImage = global::AESMS.Properties.Resources.bouton_home_menu_principal;
            this.button_Main.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_Main.Location = new System.Drawing.Point(12, 69);
            this.button_Main.Name = "button_Main";
            this.button_Main.Size = new System.Drawing.Size(52, 51);
            this.button_Main.TabIndex = 6;
            this.button_Main.UseVisualStyleBackColor = true;
            this.button_Main.Click += new System.EventHandler(this.button_Main_Click);
            // 
            // SousMenu_GestionMembre
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::AESMS.Properties.Resources.bg;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(736, 526);
            this.Controls.Add(this.button_Main);
            this.Controls.Add(this.button_Retour);
            this.Controls.Add(this.button_ImprimerCarteMembre);
            this.Controls.Add(this.button_SupprimerMembre);
            this.Controls.Add(this.button_ModifierMembre);
            this.Controls.Add(this.button_RechercherMembre);
            this.Controls.Add(this.button_AjouterMembre);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SousMenu_GestionMembre";
            this.Text = "Gestion des membres";
            this.Load += new System.EventHandler(this.SousMenu_GestionMembre_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button_AjouterMembre;
        private System.Windows.Forms.Button button_RechercherMembre;
        private System.Windows.Forms.Button button_ModifierMembre;
        private System.Windows.Forms.Button button_SupprimerMembre;
        private System.Windows.Forms.Button button_ImprimerCarteMembre;
        private System.Windows.Forms.Button button_Retour;
        private System.Windows.Forms.Button button_Main;
    }
}