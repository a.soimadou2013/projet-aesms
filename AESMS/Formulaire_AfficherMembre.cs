﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AESMS
{
    public partial class Formulaire_AfficherMembre : Form
    {
        public Formulaire_AfficherMembre()
        {
            InitializeComponent();
        }
        List<Membre> Liste_Lecture;
        Membre carte;
        int position = 0,nb=0;
        private void Formulaire_AfficherMembre_Load(object sender, EventArgs e)
        {
            //
            IFormatter format = new BinaryFormatter();
            if(System.IO.File.Exists("MembreVeiw.aesms"))
            {
                using (Stream flux = new FileStream("MembreVeiw.aesms", FileMode.Open, FileAccess.Read))
                {
                    bool ok = true;
                    int i = 0;
                    List<Membre> Lecture;
                    Liste_Lecture=Lecture = (List<Membre>)format.Deserialize(flux);
                    int cpt = 0;
                    foreach (Membre m in Lecture)
                    {
                        i++;
                        if (ok && m.getActive())
                        {
                            carte = m;
                            textBox_Nom.Text = m.getNom();
                            textBox_Prenom.Text = m.getPrenom();
                            textBox_DateNaissance.Text = m.getDateNaissance();
                            textBox_Sexe.Text = m.getSexe();
                            textBox_Formation.Text = m.getFormation();
                            textBox_Adresse.Text = m.getAdresse();
                            textBox_Telephone.Text = m.getTelephone();
                            
                            string dir = @"Image_Membre\" + m.getImage() + ".jpg";
                            string chemin = Path.GetFullPath(dir);
                            if(System.IO.File.Exists(chemin))
                            {
                                Image img = Image.FromFile(chemin);
                                pictureBox_Photo.Image = img;
                                
                            }
                            else
                            {
                                pictureBox_Photo.Image = null;
                            }
                            ok = false;
                        }
                       
                        if (!m.getActive())
                            cpt++;

                    }
                    nb = i-cpt;
                    if (nb >1 )
                    {
                        button_Suivant.Visible = true;
                    }
                    MessageBox.Show("" + nb + " résultat(s) ont été trouvé(s)","Recherche Abouti",MessageBoxButtons.OK,MessageBoxIcon.Information);
                    position++;
                }
            }
            else
            {
                MessageBox.Show("Désolé, veillez faire une recherche avant de visualiser un membre", "Resultat Echec", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void button_Precedant_Click(object sender, EventArgs e)
        {
            int i = 0;
            position-=2;
            List<Membre> Lecture=Liste_Lecture;
            foreach (Membre m in Lecture)
            {

                if (i == position && m.getActive())
                {
                   
                    textBox_Nom.Text = m.getNom();
                    textBox_Prenom.Text = m.getPrenom();
                    textBox_DateNaissance.Text = m.getDateNaissance();
                    textBox_Sexe.Text = m.getSexe();
                    textBox_Formation.Text = m.getFormation();
                    textBox_Adresse.Text = m.getAdresse();
                    textBox_Telephone.Text = m.getTelephone();
                    string dir = @"Image_Membre\" + m.getImage() + ".jpg";
                    string chemin = Path.GetFullPath(dir);
                    if(System.IO.File.Exists(chemin))
                    {
                        Image img = Image.FromFile(chemin);
                        pictureBox_Photo.Image = img;
                    }
                    else
                    {
                        pictureBox_Photo.Image = null;
                    }
                    position++;
                    carte = m;
                    break;
                }
                if(m.getActive())
                i++;
            }
            if (position <= 1)
                button_Precedant.Visible = false;
            if (position >= nb)
            {
                button_Suivant.Visible = false;
            }
            else
            {
                button_Suivant.Visible = true;
            }
        }

        private void button_GenererCarte_Click(object sender, EventArgs e)
        {
            IFormatter format = new BinaryFormatter();
            if(System.IO.File.Exists("Membre_Carte.aesms"))
            {
                System.IO.File.Delete("Membre_Carte.aesms");
            }
            using (Stream flux = new FileStream("Membre_Carte.aesms", FileMode.Create, FileAccess.Write))
            {
                format.Serialize(flux, carte);
            }
            this.Hide();
            Formulaire_GenererCarte fgenerer = new Formulaire_GenererCarte();
            fgenerer.ShowDialog();
            this.Show();
            //

        }

        private void button_Retour_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button_Main_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        private void button_AfficherOperation_Click(object sender, EventArgs e)
        {
            IFormatter format = new BinaryFormatter();
            if (System.IO.File.Exists("operationMembre.aesms"))
            {
                System.IO.File.Delete("operationMembre.aesms");
            }
            List<Operation> op = new List<Operation>();
            op = carte.getOperationMembre();
            using (Stream flux = new FileStream("operationMembre.aesms", FileMode.Create, FileAccess.Write))
            {
                format.Serialize(flux, op);
            }
            this.Hide();
            Formulaire_ConsulteMembreOperations opMembre = new Formulaire_ConsulteMembreOperations();
            opMembre.ShowDialog();
            this.Show();

        }

        private void button_Suivant_Click(object sender, EventArgs e)
        {
            int i = 0;
            List<Membre> Lecture = Liste_Lecture;
            foreach (Membre m in Lecture)
            {
                
                if (i==position && m.getActive())
                {
                    textBox_Nom.Text = m.getNom();
                    textBox_Prenom.Text = m.getPrenom();
                    textBox_DateNaissance.Text = m.getDateNaissance();
                    textBox_Sexe.Text = m.getSexe();
                    textBox_Formation.Text = m.getFormation();
                    textBox_Adresse.Text = m.getAdresse();
                    textBox_Telephone.Text = m.getTelephone();
                    string dir = @"Image_Membre\" + m.getImage() + ".jpg";
                    string chemin = Path.GetFullPath(dir);
                    if(System.IO.File.Exists(chemin))
                    {
                        Image img = Image.FromFile(chemin);
                        pictureBox_Photo.Image = img;
                    }
                    else
                    {
                        pictureBox_Photo.Image = null;
                    }
                    position++;
                    carte = m;
                    button_Precedant.Visible = true;
                    break;   
                }
                if (position <=1)
                    button_Precedant.Visible = false;
                if(m.getActive())
                i++;
                if(position+1>=nb)
                {
                    button_Suivant.Visible = false;
                }
                else
                {
                    button_Suivant.Visible = true;
                }
            }

        }
    }
}
