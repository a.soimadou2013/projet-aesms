﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AESMS
{
    public partial class SousMenu_GestionRessources : Form
    {
        public SousMenu_GestionRessources()
        {
            InitializeComponent();
        }

        private void button_ConsulterSolde_Click(object sender, EventArgs e)
        {
            Formulaire_SoldeBancaire solde = new Formulaire_SoldeBancaire();
            this.Hide();
            solde.ShowDialog();
            this.Show();
        }

        private void button_Retour_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button_Main_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        private void button_EffectuerDepot_Click(object sender, EventArgs e)
        {
            FormulaireDepotBancaire depot = new FormulaireDepotBancaire();
            this.Hide();
            depot.ShowDialog();
            this.Show();
        }

        private void button_EffectuerEmprunt_Click(object sender, EventArgs e)
        {
            Formulaire_Emprunt emprunt = new Formulaire_Emprunt();
            this.Hide();
            emprunt.ShowDialog();
            this.Show();
        }

        private void button_ConsulterOperation_Click(object sender, EventArgs e)
        {
            Formulaire_ConsultationOperation op = new Formulaire_ConsultationOperation();
            this.Hide();
            op.ShowDialog();
            this.Show();
        }

        private void SousMenu_GestionRessources_Load(object sender, EventArgs e)
        {

        }
    }
}
