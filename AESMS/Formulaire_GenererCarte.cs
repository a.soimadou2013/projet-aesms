﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AESMS
{
    public partial class Formulaire_GenererCarte : Form
    {
        int width,height;
        public Formulaire_GenererCarte()
        {
            InitializeComponent();
        }

        static Int32 position(Int32 o)
        {
            Int32 marge = 38;
            return ((o * marge) / 100);
        }
        private void printDocument_CarteAdhesion_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {

            IFormatter format = new BinaryFormatter();
            if(System.IO.File.Exists("Membre_Carte.aesms"))
            {
                Membre m;
                
                using (Stream flux = new FileStream("Membre_Carte.aesms", FileMode.Open, FileAccess.Read))
                {
                    m= (Membre)format.Deserialize(flux);
                }
                
                Bitmap bmp = Properties.Resources.carte_MODEL21;
                Image newImage = bmp, m2=Properties.Resources.File_Pictures_Icon_256 ;
                // printDocument_CarteAdhesion.DefaultPageSettings.Landscape = true;

                string dir = @"Image_Membre\" + m.getImage() + ".jpg";
                string chemin = Path.GetFullPath(dir);
                if (System.IO.File.Exists(chemin))
                {
                    Image img = Image.FromFile(chemin);
                    m2 = img;

                }

                Int32 taille = position(30);

                e.Graphics.DrawImage(newImage, 0, 0, position(newImage.Width), position(newImage.Height));
                width = position(newImage.Width);
                height = position(newImage.Height);


                e.Graphics.DrawImage(m2, 299, 69, 82, 79);

                e.Graphics.DrawString(m.getNom(), new Font("MV Boli", taille, FontStyle.Regular), Brushes.WhiteSmoke, new Point(position(215), position(139)));
                e.Graphics.DrawString(m.getPrenom(), new Font("MV Boli", taille, FontStyle.Regular), Brushes.WhiteSmoke, new Point(position(225), position(206)));
                e.Graphics.DrawString(m.getDateNaissance(), new Font("MV Boli", taille, FontStyle.Regular), Brushes.WhiteSmoke, new Point(position(310), position(271)));
                e.Graphics.DrawString("Comorienne", new Font("MV Boli", taille, FontStyle.Regular), Brushes.WhiteSmoke, new Point(position(300), position(338)));
                e.Graphics.DrawString(m.getFormation(), new Font("MV Boli", taille, FontStyle.Regular), Brushes.WhiteSmoke, new Point(position(300), position(399)));
                e.Graphics.DrawString(m.getTelephone(), new Font("MV Boli", taille, FontStyle.Regular), Brushes.WhiteSmoke, new Point(position(240), position(461)));
                
                e.Graphics.DrawString(m.getCode(), new Font("MV Boli", taille, FontStyle.Regular), Brushes.WhiteSmoke, new Point(position(720), position(554)));

                e.Graphics.DrawString("Avant " + (DateTime.Now.Day + "/" + DateTime.Now.Month + "/" + (DateTime.Now.Year + 1)), new Font("MV Boli", taille, FontStyle.Regular), Brushes.WhiteSmoke, new Point(position(220), position(553)));

                

            }


        }

        private void button_PreviewPrint_Click(object sender, EventArgs e)
        {
            printPreviewDialog_CarteAdhesion.Document = printDocument_CarteAdhesion;
            
            printPreviewDialog_CarteAdhesion.ShowDialog();
            //MessageBox.Show("Largeur: " + width + "  Longeur: " + height);
        }

        private void button_Print_Click(object sender, EventArgs e)
        {
            //printDocument_CarteAdhesion.Print();
            IFormatter format = new BinaryFormatter();
            if (System.IO.File.Exists("Membre_Carte.aesms"))
            {
                Membre m;

                using (Stream flux = new FileStream("Membre_Carte.aesms", FileMode.Open, FileAccess.Read))
                {
                    m = (Membre)format.Deserialize(flux);
                }

                bool ok = false;
                List<Membre> ecriture = new List<Membre>();
                if (System.IO.File.Exists("Liste_Carte.aesms"))
                {
                    List<Membre> lecture = new List<Membre>();


                    using (Stream flux = new FileStream("Liste_Carte.aesms", FileMode.Open, FileAccess.Read))
                    {
                        ecriture = lecture = (List<Membre>)format.Deserialize(flux);
                    }
                    foreach (Membre m2 in lecture)
                    {
                        if (m.Equals(m2) && m2.getActive())
                        {
                            ok = true;
                            break;
                        }
                    }
                    if (ok)
                    {
                        MessageBox.Show("Désolé, ce membre existe déja dans la liste d'attente d'impression !", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                    else
                    {
                        if (System.IO.File.Exists("Liste_Carte.aesms"))
                            System.IO.File.Delete("Liste_Carte.aesms");
                        ecriture.Add(m);
                        using (Stream flux = new FileStream("Liste_Carte.aesms", FileMode.OpenOrCreate, FileAccess.Write))
                        {
                            format.Serialize(flux, ecriture);
                        }
                        MessageBox.Show("Ajout éffectué avec succes dans la liste d'impression de carte d'adhésion !", "Ajout effectué", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                }
                else
                {
                    ecriture.Add(m);
                    using (Stream flux = new FileStream("Liste_Carte.aesms", FileMode.OpenOrCreate, FileAccess.Write))
                    {
                        format.Serialize(flux, ecriture);
                    }
                    MessageBox.Show("Ajout éffectué avec succes dans la liste d'impression de carte d'adhésion !", "Ajout effectué", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }

            }
        }

        private void button_Retour_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button_Main_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        private void Formulaire_GenererCarte_Load(object sender, EventArgs e)
        {
            //
        }

 
    }
}
