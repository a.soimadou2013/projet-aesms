﻿namespace AESMS
{
    partial class Formulaire_GenererCarte
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Formulaire_GenererCarte));
            this.button_Print = new System.Windows.Forms.Button();
            this.button_PreviewPrint = new System.Windows.Forms.Button();
            this.printDocument_CarteAdhesion = new System.Drawing.Printing.PrintDocument();
            this.printPreviewDialog_CarteAdhesion = new System.Windows.Forms.PrintPreviewDialog();
            this.button_Retour = new System.Windows.Forms.Button();
            this.button_Main = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button_Print
            // 
            this.button_Print.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Print.Image = global::AESMS.Properties.Resources.Hardware_Printer_Blue_Icon_256;
            this.button_Print.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button_Print.Location = new System.Drawing.Point(508, 59);
            this.button_Print.Name = "button_Print";
            this.button_Print.Size = new System.Drawing.Size(267, 263);
            this.button_Print.TabIndex = 44;
            this.button_Print.Text = "Générer la carte d\'adhésion";
            this.button_Print.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button_Print.UseVisualStyleBackColor = true;
            this.button_Print.Click += new System.EventHandler(this.button_Print_Click);
            // 
            // button_PreviewPrint
            // 
            this.button_PreviewPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_PreviewPrint.Image = global::AESMS.Properties.Resources.Preview_Icon_256;
            this.button_PreviewPrint.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button_PreviewPrint.Location = new System.Drawing.Point(12, 59);
            this.button_PreviewPrint.Name = "button_PreviewPrint";
            this.button_PreviewPrint.Size = new System.Drawing.Size(267, 263);
            this.button_PreviewPrint.TabIndex = 43;
            this.button_PreviewPrint.Text = "Prévisualiser la carte d\'adhésion";
            this.button_PreviewPrint.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button_PreviewPrint.UseVisualStyleBackColor = true;
            this.button_PreviewPrint.Click += new System.EventHandler(this.button_PreviewPrint_Click);
            // 
            // printDocument_CarteAdhesion
            // 
            this.printDocument_CarteAdhesion.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument_CarteAdhesion_PrintPage);
            // 
            // printPreviewDialog_CarteAdhesion
            // 
            this.printPreviewDialog_CarteAdhesion.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog_CarteAdhesion.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog_CarteAdhesion.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog_CarteAdhesion.Enabled = true;
            this.printPreviewDialog_CarteAdhesion.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog_CarteAdhesion.Icon")));
            this.printPreviewDialog_CarteAdhesion.Name = "printPreviewDialog_CarteAdhesion";
            this.printPreviewDialog_CarteAdhesion.Visible = false;
            // 
            // button_Retour
            // 
            this.button_Retour.BackgroundImage = global::AESMS.Properties.Resources.Alarm_Arrow_Left_Icon_48;
            this.button_Retour.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_Retour.Location = new System.Drawing.Point(12, 2);
            this.button_Retour.Name = "button_Retour";
            this.button_Retour.Size = new System.Drawing.Size(52, 51);
            this.button_Retour.TabIndex = 45;
            this.button_Retour.UseVisualStyleBackColor = true;
            this.button_Retour.Click += new System.EventHandler(this.button_Retour_Click);
            // 
            // button_Main
            // 
            this.button_Main.BackgroundImage = global::AESMS.Properties.Resources.bouton_home_menu_principal;
            this.button_Main.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_Main.Location = new System.Drawing.Point(70, 2);
            this.button_Main.Name = "button_Main";
            this.button_Main.Size = new System.Drawing.Size(52, 51);
            this.button_Main.TabIndex = 46;
            this.button_Main.UseVisualStyleBackColor = true;
            this.button_Main.Click += new System.EventHandler(this.button_Main_Click);
            // 
            // Formulaire_GenererCarte
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::AESMS.Properties.Resources.bg;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(787, 458);
            this.Controls.Add(this.button_Main);
            this.Controls.Add(this.button_Retour);
            this.Controls.Add(this.button_Print);
            this.Controls.Add(this.button_PreviewPrint);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Formulaire_GenererCarte";
            this.Text = "Formulaire de génération de carte d\'adhésion";
            this.Load += new System.EventHandler(this.Formulaire_GenererCarte_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button_Print;
        private System.Windows.Forms.Button button_PreviewPrint;
        private System.Drawing.Printing.PrintDocument printDocument_CarteAdhesion;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog_CarteAdhesion;
        private System.Windows.Forms.Button button_Retour;
        private System.Windows.Forms.Button button_Main;
    }
}