﻿namespace AESMS
{
    partial class Formulaire_ConsultationOperation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Formulaire_ConsultationOperation));
            this.button_Main = new System.Windows.Forms.Button();
            this.button_Retour = new System.Windows.Forms.Button();
            this.button_Precedant = new System.Windows.Forms.Button();
            this.button_Suivant = new System.Windows.Forms.Button();
            this.textBox_MontantOperation = new System.Windows.Forms.TextBox();
            this.label_MontantOperation = new System.Windows.Forms.Label();
            this.textBox_Restant = new System.Windows.Forms.TextBox();
            this.label_MontantRestant = new System.Windows.Forms.Label();
            this.label_DateNaissance = new System.Windows.Forms.Label();
            this.textBox_TypeOperation = new System.Windows.Forms.TextBox();
            this.label_Type = new System.Windows.Forms.Label();
            this.textBox_ID = new System.Windows.Forms.TextBox();
            this.label_ID = new System.Windows.Forms.Label();
            this.textBox_Date = new System.Windows.Forms.TextBox();
            this.button_MembreEmprunt = new System.Windows.Forms.Button();
            this.button_RembourserEmprunt = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button_Main
            // 
            this.button_Main.BackgroundImage = global::AESMS.Properties.Resources.bouton_home_menu_principal;
            this.button_Main.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_Main.Location = new System.Drawing.Point(22, 88);
            this.button_Main.Name = "button_Main";
            this.button_Main.Size = new System.Drawing.Size(52, 51);
            this.button_Main.TabIndex = 81;
            this.button_Main.UseVisualStyleBackColor = true;
            this.button_Main.Click += new System.EventHandler(this.button_Main_Click);
            // 
            // button_Retour
            // 
            this.button_Retour.BackgroundImage = global::AESMS.Properties.Resources.Alarm_Arrow_Left_Icon_48;
            this.button_Retour.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_Retour.Location = new System.Drawing.Point(22, 31);
            this.button_Retour.Name = "button_Retour";
            this.button_Retour.Size = new System.Drawing.Size(52, 51);
            this.button_Retour.TabIndex = 80;
            this.button_Retour.UseVisualStyleBackColor = true;
            this.button_Retour.Click += new System.EventHandler(this.button_Retour_Click);
            // 
            // button_Precedant
            // 
            this.button_Precedant.Image = global::AESMS.Properties.Resources.Alarm_Arrow_Left_Icon_96;
            this.button_Precedant.Location = new System.Drawing.Point(492, 351);
            this.button_Precedant.Name = "button_Precedant";
            this.button_Precedant.Size = new System.Drawing.Size(121, 100);
            this.button_Precedant.TabIndex = 78;
            this.button_Precedant.UseVisualStyleBackColor = true;
            this.button_Precedant.Visible = false;
            this.button_Precedant.Click += new System.EventHandler(this.button_Precedant_Click);
            // 
            // button_Suivant
            // 
            this.button_Suivant.Image = global::AESMS.Properties.Resources.Alarm_Arrow_Right_Icon_96;
            this.button_Suivant.Location = new System.Drawing.Point(619, 351);
            this.button_Suivant.Name = "button_Suivant";
            this.button_Suivant.Size = new System.Drawing.Size(121, 100);
            this.button_Suivant.TabIndex = 77;
            this.button_Suivant.UseVisualStyleBackColor = true;
            this.button_Suivant.Visible = false;
            this.button_Suivant.Click += new System.EventHandler(this.button_Suivant_Click);
            // 
            // textBox_MontantOperation
            // 
            this.textBox_MontantOperation.Enabled = false;
            this.textBox_MontantOperation.Location = new System.Drawing.Point(353, 192);
            this.textBox_MontantOperation.Name = "textBox_MontantOperation";
            this.textBox_MontantOperation.Size = new System.Drawing.Size(176, 20);
            this.textBox_MontantOperation.TabIndex = 76;
            // 
            // label_MontantOperation
            // 
            this.label_MontantOperation.AutoSize = true;
            this.label_MontantOperation.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label_MontantOperation.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label_MontantOperation.Location = new System.Drawing.Point(80, 190);
            this.label_MontantOperation.Name = "label_MontantOperation";
            this.label_MontantOperation.Size = new System.Drawing.Size(171, 20);
            this.label_MontantOperation.TabIndex = 73;
            this.label_MontantOperation.Text = "Montant de l\'opération:";
            // 
            // textBox_Restant
            // 
            this.textBox_Restant.Enabled = false;
            this.textBox_Restant.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBox_Restant.Location = new System.Drawing.Point(353, 238);
            this.textBox_Restant.Name = "textBox_Restant";
            this.textBox_Restant.Size = new System.Drawing.Size(176, 26);
            this.textBox_Restant.TabIndex = 68;
            // 
            // label_MontantRestant
            // 
            this.label_MontantRestant.AutoSize = true;
            this.label_MontantRestant.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label_MontantRestant.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label_MontantRestant.Location = new System.Drawing.Point(80, 241);
            this.label_MontantRestant.Name = "label_MontantRestant";
            this.label_MontantRestant.Size = new System.Drawing.Size(224, 20);
            this.label_MontantRestant.TabIndex = 67;
            this.label_MontantRestant.Text = "Montant Total dans la banque:";
            // 
            // label_DateNaissance
            // 
            this.label_DateNaissance.AutoSize = true;
            this.label_DateNaissance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label_DateNaissance.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label_DateNaissance.Location = new System.Drawing.Point(80, 144);
            this.label_DateNaissance.Name = "label_DateNaissance";
            this.label_DateNaissance.Size = new System.Drawing.Size(124, 20);
            this.label_DateNaissance.TabIndex = 66;
            this.label_DateNaissance.Text = "Date réalisation:";
            // 
            // textBox_TypeOperation
            // 
            this.textBox_TypeOperation.Enabled = false;
            this.textBox_TypeOperation.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBox_TypeOperation.Location = new System.Drawing.Point(353, 93);
            this.textBox_TypeOperation.Name = "textBox_TypeOperation";
            this.textBox_TypeOperation.Size = new System.Drawing.Size(176, 26);
            this.textBox_TypeOperation.TabIndex = 65;
            this.textBox_TypeOperation.TextChanged += new System.EventHandler(this.textBox_TypeOperation_TextChanged);
            // 
            // label_Type
            // 
            this.label_Type.AutoSize = true;
            this.label_Type.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label_Type.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label_Type.Location = new System.Drawing.Point(80, 96);
            this.label_Type.Name = "label_Type";
            this.label_Type.Size = new System.Drawing.Size(130, 20);
            this.label_Type.TabIndex = 64;
            this.label_Type.Text = "Type d\'opération:";
            // 
            // textBox_ID
            // 
            this.textBox_ID.Enabled = false;
            this.textBox_ID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBox_ID.Location = new System.Drawing.Point(353, 44);
            this.textBox_ID.Name = "textBox_ID";
            this.textBox_ID.Size = new System.Drawing.Size(176, 26);
            this.textBox_ID.TabIndex = 63;
            // 
            // label_ID
            // 
            this.label_ID.AutoSize = true;
            this.label_ID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label_ID.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label_ID.Location = new System.Drawing.Point(80, 44);
            this.label_ID.Name = "label_ID";
            this.label_ID.Size = new System.Drawing.Size(30, 20);
            this.label_ID.TabIndex = 62;
            this.label_ID.Text = "ID:";
            // 
            // textBox_Date
            // 
            this.textBox_Date.Enabled = false;
            this.textBox_Date.Location = new System.Drawing.Point(353, 149);
            this.textBox_Date.Name = "textBox_Date";
            this.textBox_Date.Size = new System.Drawing.Size(176, 20);
            this.textBox_Date.TabIndex = 82;
            // 
            // button_MembreEmprunt
            // 
            this.button_MembreEmprunt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_MembreEmprunt.Location = new System.Drawing.Point(-2, 351);
            this.button_MembreEmprunt.Name = "button_MembreEmprunt";
            this.button_MembreEmprunt.Size = new System.Drawing.Size(223, 100);
            this.button_MembreEmprunt.TabIndex = 84;
            this.button_MembreEmprunt.Text = "Voir le membre qui a efféctué l\'emprunt";
            this.button_MembreEmprunt.UseVisualStyleBackColor = true;
            this.button_MembreEmprunt.Visible = false;
            this.button_MembreEmprunt.Click += new System.EventHandler(this.button_MembreEmprunt_Click);
            // 
            // button_RembourserEmprunt
            // 
            this.button_RembourserEmprunt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_RembourserEmprunt.ForeColor = System.Drawing.Color.Maroon;
            this.button_RembourserEmprunt.Location = new System.Drawing.Point(619, 192);
            this.button_RembourserEmprunt.Name = "button_RembourserEmprunt";
            this.button_RembourserEmprunt.Size = new System.Drawing.Size(121, 75);
            this.button_RembourserEmprunt.TabIndex = 85;
            this.button_RembourserEmprunt.Text = "Rembourser l\'emprunt";
            this.button_RembourserEmprunt.UseVisualStyleBackColor = true;
            this.button_RembourserEmprunt.Click += new System.EventHandler(this.button_RembourserEmprunt_Click);
            // 
            // Formulaire_ConsultationOperation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::AESMS.Properties.Resources.bg;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(752, 463);
            this.Controls.Add(this.button_RembourserEmprunt);
            this.Controls.Add(this.button_MembreEmprunt);
            this.Controls.Add(this.textBox_Date);
            this.Controls.Add(this.button_Main);
            this.Controls.Add(this.button_Retour);
            this.Controls.Add(this.button_Precedant);
            this.Controls.Add(this.button_Suivant);
            this.Controls.Add(this.textBox_MontantOperation);
            this.Controls.Add(this.label_MontantOperation);
            this.Controls.Add(this.textBox_Restant);
            this.Controls.Add(this.label_MontantRestant);
            this.Controls.Add(this.label_DateNaissance);
            this.Controls.Add(this.textBox_TypeOperation);
            this.Controls.Add(this.label_Type);
            this.Controls.Add(this.textBox_ID);
            this.Controls.Add(this.label_ID);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Formulaire_ConsultationOperation";
            this.Text = "Formulaire de consultation des operations";
            this.Load += new System.EventHandler(this.Formulaire_ConsultationOperation_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_Main;
        private System.Windows.Forms.Button button_Retour;
        private System.Windows.Forms.Button button_Precedant;
        private System.Windows.Forms.Button button_Suivant;
        private System.Windows.Forms.TextBox textBox_MontantOperation;
        private System.Windows.Forms.Label label_MontantOperation;
        private System.Windows.Forms.TextBox textBox_Restant;
        private System.Windows.Forms.Label label_MontantRestant;
        private System.Windows.Forms.Label label_DateNaissance;
        private System.Windows.Forms.TextBox textBox_TypeOperation;
        private System.Windows.Forms.Label label_Type;
        private System.Windows.Forms.TextBox textBox_ID;
        private System.Windows.Forms.Label label_ID;
        private System.Windows.Forms.TextBox textBox_Date;
        private System.Windows.Forms.Button button_MembreEmprunt;
        private System.Windows.Forms.Button button_RembourserEmprunt;
    }
}