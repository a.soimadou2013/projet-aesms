﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AESMS
{
    [Serializable]
    class Operation
    {
        private long id;
        private int typeOperation;
        private int montantOperation;
        private int montantRestant;
        private DateTime date;
        private Membre m = new Membre();
        private bool Etat_Emprunt;

        public Operation()
        {
            id = 0;
            typeOperation = 0;
            montantOperation = 0;
            montantRestant = 0;
           
        }
        public bool getEtatEmprunt()
        {
            return Etat_Emprunt;
        }
        public Membre getMembre()
        {
            return m;
        }
        public long getId()
        {
            return id;
        }
        public int getTypeOperation()
        {
            return typeOperation;
        }
        public int getMontantOperation()
        {
            return montantOperation;
        }
        public int getMontantRestant()
        {
            return montantRestant;
        }
        public DateTime getDate()
        {
            return date;
        }

        public void setMembre(Membre m)
        {
            this.m = m;
        }
        public void setId(long id)
        {
            this.id = id;
        }
        public void setTypeOperation(int typeOperation)
        {
            this.typeOperation=typeOperation;
        }
        public void setMontantOperation(int montantOperation)
        {
            this.montantOperation=montantOperation;
        }
        public void setMontantRestant(int montantRestant)
        {
            this.montantRestant=montantRestant;
        }
        public void setDate(DateTime date)
        {
            this.date = date;
        }
        public void setEtatEmprunt(bool Etat_Emprunt)
        {
            this.Etat_Emprunt = Etat_Emprunt;
        }
    }
}
