﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AESMS
{
    public partial class Formulaire_Emprunt : Form
    {
        public Formulaire_Emprunt()
        {
            InitializeComponent();
        }
        bool EffectuerEmprunt_Clic = false;
        Membre membreTrouve = new Membre();
        private void Formulaire_Emprunt_Load(object sender, EventArgs e)
        {
            //
            IFormatter format = new BinaryFormatter();
            button_Continuer.Text = "Continuer";
            comboBox_ListeMembre.Visible = true;
            label_MontantEmprunt.Visible = false;
            textBox_MontantEmprunt.Visible = false;
            EffectuerEmprunt_Clic = false;
            button_EffectuerEmprunt.Visible = false;
            button_Continuer.Visible = true;

            comboBox_ListeMembre.Text = "Liste des membres de l'association AESMS";

            if (System.IO.File.Exists("MembreVeiw.aesms"))
                System.IO.File.Delete("MembreVeiw.aesms");

            if (System.IO.File.Exists("Membre.aesms"))
            {
                using (Stream flux = new FileStream("Membre.aesms", FileMode.Open, FileAccess.Read))
                {
                    List<Membre> Liste_Lecture;
                    Liste_Lecture = (List<Membre>)format.Deserialize(flux);
                    int i = 1;
                    foreach (Membre m in Liste_Lecture)
                    {
                        if (m.getActive())
                        {
                            comboBox_ListeMembre.Items.Add("" + i + " - " + m.getNom() + " " + m.getPrenom() + " code:" + m.getCode());
                            i++;
                        }

                    }
                }

            }
            else
            {
                comboBox_ListeMembre.Items.Add("Il n'y a aucun membre ajouté dans le système !");
            }
        }

        private void button_Continuer_Click(object sender, EventArgs e)
        {
            if (System.IO.File.Exists("MembreVeiw.aesms"))
                System.IO.File.Delete("MembreVeiw.aesms");

            if (comboBox_ListeMembre.Text.Equals("Liste des membres de l'association AESMS") || comboBox_ListeMembre.Text.Equals("Il n'y a aucun membre ajouté dans le système !"))
            {
                MessageBox.Show("Veillez selectionner un membre avant de continuer !", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                string ch = "", ch2 = comboBox_ListeMembre.Text;
                int cpt = 0;
                bool ok = false;
                for (int i = 0; i < ch2.Length; i++)
                {
                    if (ch2[i] == ' ' && cpt < 3)
                        cpt++;
                    if (cpt > 2)
                    {
                        if (ch2[i] == ' ')
                        {
                            if (ch2[i + 1] >= '0' && ch2[i + 1] <= '9')
                                break;
                        }

                        ch += ch2[i];
                    }
                    if (cpt == 2)
                        cpt++;

                }
                ch2 = ch;

                cpt = 0;
                ch = "";
                string code = "";
                for (int i = 0; i < ch2.Length; i++)
                {
                    ch = "";
                    if (ch2[i] == ' ' && ok == false)
                    {
                        ch = "" + ch2[i] + ch2[i + 1] + ch2[i + 2] + ch2[i + 3] + ch2[i + 4] + ch2[i + 5];
                        if (ch.Equals(" code:"))
                        {
                            i = i + 6;
                            ok = true;
                        }
                    }

                    if (ok)
                    {
                        code += ch2[i];

                    }
                }
                bool trouve = false;

                if (System.IO.File.Exists("Membre.aesms"))
                {
                    IFormatter format = new BinaryFormatter();
                    

                    List<Membre> ecriture = new List<Membre>();
                    using (Stream flux = new FileStream("Membre.aesms", FileMode.Open, FileAccess.Read))
                    {
                        List<Membre> Liste_Lecture;
                        List<Membre> Liste_Trouver = new List<Membre>();



                        Liste_Lecture = (List<Membre>)format.Deserialize(flux);
                        foreach (Membre m in Liste_Lecture)
                        {

                            if (code.Equals(m.getCode()) && m.getActive())
                            {

                                ecriture.Add(m);
                                trouve = true;
                                membreTrouve = m;
                                break;
                            }


                        }
                    }
                    if (trouve)
                    {
                        /*
                        using (Stream flux = new FileStream("MembreVeiw.aesms", FileMode.Create, FileAccess.Write))
                        {
                            format.Serialize(flux, ecriture);
                        }
                        M+essageBox.Show("Membre supprimé avec succes !", "Suppression effectué", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        */
                        EffectuerEmprunt_Clic = true;
                        comboBox_ListeMembre.Items.Clear();
                        comboBox_ListeMembre.Visible = false;
                        label_MontantEmprunt.Visible = true;
                        textBox_MontantEmprunt.Visible = true;
                        button_Continuer.Visible = false;
                        //DEPUT DE L'ALGORITHME D'EMPRUNT
                        button_EffectuerEmprunt.Visible = true;
                        
                        
                    }
                    else
                    {
                        MessageBox.Show("Désolé, ce membre n'existe pas !", "Membre introuvable", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }

                }
                else
                {
                    MessageBox.Show("Attention, le fichier Membre.aesms est introuvable !", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }

            ;
        }

        private void button_EffectuerEmprunt_Click(object sender, EventArgs e)
        {
            IFormatter format = new BinaryFormatter();
            

            if (EffectuerEmprunt_Clic)
            {

                Operation op = new Operation();
                CompteBancaire compte = new CompteBancaire();
                List<Operation> liste_Op = new List<Operation>();
                List<Operation> temp = new List<Operation>();
                long cptop = 1;

                string str = textBox_MontantEmprunt.Text;
                int m = 0;
                bool continuer = true;
                int.TryParse(str, out m);
                if (!m.Equals(0))
                {
                    if (System.IO.File.Exists("operation.aesms"))
                    {
                        using (Stream flux = new FileStream("operation.aesms", FileMode.Open, FileAccess.Read))
                        {
                            temp = (List<Operation>)format.Deserialize(flux);
                        }
                        liste_Op = temp;
                        cptop = 0;
                        foreach (Operation o in temp)
                        {
                            cptop++;
                        }
                        cptop++;
                       
                        op.setId(cptop);
                        op.setTypeOperation(2);
                        DateTime date = DateTime.Now.Date;
                        op.setDate(date);
                        op.setEtatEmprunt(true);

                        string ch3 = "";
                        ch3 = textBox_MontantEmprunt.Text;
                        int montantEmprunt = int.Parse(ch3);
                        op.setMontantOperation(montantEmprunt);
                        if (System.IO.File.Exists("ressources.aesms"))
                        {
                            using (Stream flux = new FileStream("ressources.aesms", FileMode.Open, FileAccess.Read))
                            {
                                compte = (CompteBancaire)format.Deserialize(flux);
                            }
                        }
                        if ((compte.getMontant() - montantEmprunt) > 0)
                        {
                            op.setMontantRestant(compte.getMontant() - montantEmprunt);
                            compte.setMontant(op.getMontantRestant());


                            //Transfert des operation chez le membre

                            
                            if (System.IO.File.Exists("Membre.aesms"))
                            {

                                bool trouve = false;
                                List<Membre> ecriture = new List<Membre>();
                                using (Stream flux = new FileStream("Membre.aesms", FileMode.Open, FileAccess.Read))
                                {
                                    List<Membre> Liste_Lecture;
                                    List<Membre> Liste_Trouver = new List<Membre>();


                                    Liste_Lecture = (List<Membre>)format.Deserialize(flux);
                                    foreach (Membre m2 in Liste_Lecture)
                                    {

                                        if (membreTrouve.getCode().Equals(m2.getCode()) && m2.getActive())
                                        {
                                            List<Operation> opMembre = m2.getOperationMembre();
                                            opMembre.Add(op);
                                            m2.setOperationMembre(opMembre);
                                            ecriture.Add(m2);
                                            trouve = true;
                                        }
                                        else
                                        {
                                            ecriture.Add(m2);
                                        }

                                    }
                                }
                                if (trouve)
                                {
                                    if (System.IO.File.Exists("Membre.aesms"))
                                    {
                                        System.IO.File.Delete("Membre.aesms");
                                    }
                                    using (Stream flux = new FileStream("Membre.aesms", FileMode.Create, FileAccess.Write))
                                    {
                                        format.Serialize(flux, ecriture);
                                    }
                                    comboBox_ListeMembre.Items.Clear();

                                }
                                else
                                {
                                    MessageBox.Show("Désolé, ce membre n'existe pas !", "Membre introuvable", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                }
                            }
                            else
                            {
                                continuer = false;
                            }
                            op.setMembre(membreTrouve);


                            liste_Op.Add(op);
                        }
                        else
                        {
                            continuer = false;
                        }
                    }
                    else
                    {
                        op.setId(cptop);
                        op.setTypeOperation(1);
                        DateTime date = DateTime.Now;
                        op.setDate(date);
                        op.setEtatEmprunt(true);
                        string ch3 = "";
                        ch3 = textBox_MontantEmprunt.Text;
                        int montantEmprunt = int.Parse(ch3);
                        op.setMontantOperation(montantEmprunt);
                        if (System.IO.File.Exists("ressources.aesms"))
                        {
                            using (Stream flux = new FileStream("ressources.aesms", FileMode.Open, FileAccess.Read))
                            {
                                compte = (CompteBancaire)format.Deserialize(flux);
                            }
                        }
                        if ((compte.getMontant() - montantEmprunt) > 0)
                        {
                            op.setMontantRestant(compte.getMontant() - montantEmprunt);
                            compte.setMontant(op.getMontantRestant());

                            //Transfert des operation chez le membre

                            if (System.IO.File.Exists("Membre.aesms"))
                            {

                                bool trouve = false;
                                List<Membre> ecriture = new List<Membre>();
                                using (Stream flux = new FileStream("Membre.aesms", FileMode.Open, FileAccess.Read))
                                {
                                    List<Membre> Liste_Lecture;
                                    List<Membre> Liste_Trouver = new List<Membre>();


                                    Liste_Lecture = (List<Membre>)format.Deserialize(flux);
                                    foreach (Membre m2 in Liste_Lecture)
                                    {

                                        if (membreTrouve.getCode().Equals(m2.getCode()) && m2.getActive())
                                        {
                                            List<Operation> opMembre = m2.getOperationMembre();
                                            opMembre.Add(op);
                                            m2.setOperationMembre(opMembre);
                                            ecriture.Add(m2);
                                            trouve = true;
                                        }
                                        else
                                        {
                                            ecriture.Add(m2);
                                        }

                                    }
                                }
                                if (trouve)
                                {
                                    if (System.IO.File.Exists("Membre.aesms"))
                                    {
                                        System.IO.File.Delete("Membre.aesms");
                                    }
                                    using (Stream flux = new FileStream("Membre.aesms", FileMode.Create, FileAccess.Write))
                                    {
                                        format.Serialize(flux, ecriture);
                                    }
                                    comboBox_ListeMembre.Items.Clear();

                                }
                                else
                                {
                                    MessageBox.Show("Désolé, ce membre n'existe pas !", "Membre introuvable", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                }
                            }
                            else
                            {
                                continuer = false;
                            }
                            op.setMembre(membreTrouve);

                            liste_Op.Add(op);
                        }
                        else
                        {
                            continuer = false;
                        }

                    }
                    if (continuer)
                    {
                        if (System.IO.File.Exists("ressources.aesms"))
                            System.IO.File.Delete("ressources.aesms");

                        using (Stream flux = new FileStream("ressources.aesms", FileMode.Create, FileAccess.Write))
                        {
                            format.Serialize(flux, compte);
                        }

                        if (System.IO.File.Exists("operation.aesms"))
                            System.IO.File.Delete("operation.aesms");
                        using (Stream flux = new FileStream("operation.aesms", FileMode.Create, FileAccess.Write))
                        {
                            format.Serialize(flux, liste_Op);
                        }
                        MessageBox.Show("Emprunt éffectué avec succès !", "Emprunt éffectué", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                    else
                    {
                        MessageBox.Show("Désolé, ce montant dépasse  le montant restant dans la caisse !", "Montant suppérieur au caisse de l'association", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }

                }
                else
                {
                    MessageBox.Show("Désolé, ce montant est insuffisant , veillez saisir un montant supérieur a 0 !", "Echec de l'emprunt", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    textBox_MontantEmprunt.Text = "";
                    textBox_MontantEmprunt.Focus();
                }
            }
            Formulaire_Emprunt_Load(sender, e);
            EffectuerEmprunt_Clic = false;
            button_EffectuerEmprunt.Visible = false;
            button_Continuer.Visible = true;

        }

        private void button_Retour_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button_Main_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }
    }
}
