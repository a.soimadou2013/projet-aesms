﻿namespace AESMS
{
    partial class Formulaire_AjoutMembre
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Formulaire_AjoutMembre));
            this.button_Pivoter = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBox_Sexe = new System.Windows.Forms.ComboBox();
            this.label_Sexe = new System.Windows.Forms.Label();
            this.dateTimePicker_DateNaissance = new System.Windows.Forms.DateTimePicker();
            this.textBox_Telephone = new System.Windows.Forms.TextBox();
            this.label_Telephone = new System.Windows.Forms.Label();
            this.textBox_Adresse = new System.Windows.Forms.TextBox();
            this.textBox_Formation = new System.Windows.Forms.TextBox();
            this.label_Formation = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label_DateNaissance = new System.Windows.Forms.Label();
            this.textBox_Prenom = new System.Windows.Forms.TextBox();
            this.label_Prenom = new System.Windows.Forms.Label();
            this.textBox_Nom = new System.Windows.Forms.TextBox();
            this.label_Nom = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox_Photo = new System.Windows.Forms.PictureBox();
            this.button_Ajouter = new System.Windows.Forms.Button();
            this.button_PreviewPrint = new System.Windows.Forms.Button();
            this.button_NouvelleAjout = new System.Windows.Forms.Button();
            this.printPreviewDialog_CarteAdhesion = new System.Windows.Forms.PrintPreviewDialog();
            this.printDocument_CarteAdhesion = new System.Drawing.Printing.PrintDocument();
            this.button_Retour = new System.Windows.Forms.Button();
            this.button_Main = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Photo)).BeginInit();
            this.SuspendLayout();
            // 
            // button_Pivoter
            // 
            this.button_Pivoter.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button_Pivoter.Location = new System.Drawing.Point(524, 292);
            this.button_Pivoter.Name = "button_Pivoter";
            this.button_Pivoter.Size = new System.Drawing.Size(87, 50);
            this.button_Pivoter.TabIndex = 47;
            this.button_Pivoter.Text = "Faire pivoter l\'image";
            this.button_Pivoter.UseVisualStyleBackColor = true;
            this.button_Pivoter.Click += new System.EventHandler(this.button_Pivoter_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.label2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label2.Location = new System.Drawing.Point(596, 373);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(214, 16);
            this.label2.TabIndex = 45;
            this.label2.Text = "cliquer pour ajouter une photo";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // comboBox_Sexe
            // 
            this.comboBox_Sexe.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.comboBox_Sexe.FormattingEnabled = true;
            this.comboBox_Sexe.Items.AddRange(new object[] {
            "Masculin",
            "Féminin"});
            this.comboBox_Sexe.Location = new System.Drawing.Point(310, 268);
            this.comboBox_Sexe.Name = "comboBox_Sexe";
            this.comboBox_Sexe.Size = new System.Drawing.Size(158, 28);
            this.comboBox_Sexe.TabIndex = 39;
            this.comboBox_Sexe.Text = "- Choisissez ici";
            this.comboBox_Sexe.SelectedIndexChanged += new System.EventHandler(this.comboBox_Sexe_SelectedIndexChanged);
            // 
            // label_Sexe
            // 
            this.label_Sexe.AutoSize = true;
            this.label_Sexe.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label_Sexe.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label_Sexe.Location = new System.Drawing.Point(78, 271);
            this.label_Sexe.Name = "label_Sexe";
            this.label_Sexe.Size = new System.Drawing.Size(49, 20);
            this.label_Sexe.TabIndex = 38;
            this.label_Sexe.Text = "Sexe:";
            // 
            // dateTimePicker_DateNaissance
            // 
            this.dateTimePicker_DateNaissance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.dateTimePicker_DateNaissance.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker_DateNaissance.Location = new System.Drawing.Point(303, 220);
            this.dateTimePicker_DateNaissance.Name = "dateTimePicker_DateNaissance";
            this.dateTimePicker_DateNaissance.Size = new System.Drawing.Size(165, 26);
            this.dateTimePicker_DateNaissance.TabIndex = 37;
            // 
            // textBox_Telephone
            // 
            this.textBox_Telephone.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBox_Telephone.Location = new System.Drawing.Point(290, 418);
            this.textBox_Telephone.Name = "textBox_Telephone";
            this.textBox_Telephone.Size = new System.Drawing.Size(178, 26);
            this.textBox_Telephone.TabIndex = 36;
            // 
            // label_Telephone
            // 
            this.label_Telephone.AutoSize = true;
            this.label_Telephone.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label_Telephone.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label_Telephone.Location = new System.Drawing.Point(78, 421);
            this.label_Telephone.Name = "label_Telephone";
            this.label_Telephone.Size = new System.Drawing.Size(112, 20);
            this.label_Telephone.TabIndex = 35;
            this.label_Telephone.Text = "No Téléphone:";
            // 
            // textBox_Adresse
            // 
            this.textBox_Adresse.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBox_Adresse.Location = new System.Drawing.Point(236, 367);
            this.textBox_Adresse.Name = "textBox_Adresse";
            this.textBox_Adresse.Size = new System.Drawing.Size(233, 26);
            this.textBox_Adresse.TabIndex = 34;
            // 
            // textBox_Formation
            // 
            this.textBox_Formation.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBox_Formation.Location = new System.Drawing.Point(264, 319);
            this.textBox_Formation.Name = "textBox_Formation";
            this.textBox_Formation.Size = new System.Drawing.Size(205, 26);
            this.textBox_Formation.TabIndex = 32;
            // 
            // label_Formation
            // 
            this.label_Formation.AutoSize = true;
            this.label_Formation.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label_Formation.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label_Formation.Location = new System.Drawing.Point(78, 322);
            this.label_Formation.Name = "label_Formation";
            this.label_Formation.Size = new System.Drawing.Size(85, 20);
            this.label_Formation.TabIndex = 31;
            this.label_Formation.Text = "Formation:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label6.Location = new System.Drawing.Point(78, 370);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 20);
            this.label6.TabIndex = 33;
            this.label6.Text = "Adresse:";
            // 
            // label_DateNaissance
            // 
            this.label_DateNaissance.AutoSize = true;
            this.label_DateNaissance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label_DateNaissance.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label_DateNaissance.Location = new System.Drawing.Point(78, 225);
            this.label_DateNaissance.Name = "label_DateNaissance";
            this.label_DateNaissance.Size = new System.Drawing.Size(146, 20);
            this.label_DateNaissance.TabIndex = 30;
            this.label_DateNaissance.Text = "Date de naissance:";
            // 
            // textBox_Prenom
            // 
            this.textBox_Prenom.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBox_Prenom.Location = new System.Drawing.Point(264, 171);
            this.textBox_Prenom.Name = "textBox_Prenom";
            this.textBox_Prenom.Size = new System.Drawing.Size(205, 26);
            this.textBox_Prenom.TabIndex = 29;
            // 
            // label_Prenom
            // 
            this.label_Prenom.AutoSize = true;
            this.label_Prenom.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label_Prenom.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label_Prenom.Location = new System.Drawing.Point(78, 177);
            this.label_Prenom.Name = "label_Prenom";
            this.label_Prenom.Size = new System.Drawing.Size(68, 20);
            this.label_Prenom.TabIndex = 28;
            this.label_Prenom.Text = "Prénom:";
            // 
            // textBox_Nom
            // 
            this.textBox_Nom.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBox_Nom.Location = new System.Drawing.Point(264, 122);
            this.textBox_Nom.Name = "textBox_Nom";
            this.textBox_Nom.Size = new System.Drawing.Size(205, 26);
            this.textBox_Nom.TabIndex = 27;
            // 
            // label_Nom
            // 
            this.label_Nom.AutoSize = true;
            this.label_Nom.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label_Nom.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label_Nom.Location = new System.Drawing.Point(78, 125);
            this.label_Nom.Name = "label_Nom";
            this.label_Nom.Size = new System.Drawing.Size(46, 20);
            this.label_Nom.TabIndex = 26;
            this.label_Nom.Text = "Nom:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label1.Font = new System.Drawing.Font("Lucida Console", 36F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))));
            this.label1.ForeColor = System.Drawing.Color.Maroon;
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(43, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(680, 48);
            this.label1.TabIndex = 25;
            this.label1.Text = "ESPACE AJOUT DE MEMBRE";
            // 
            // pictureBox_Photo
            // 
            this.pictureBox_Photo.BackgroundImage = global::AESMS.Properties.Resources.File_Pictures_Icon_256;
            this.pictureBox_Photo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox_Photo.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pictureBox_Photo.Location = new System.Drawing.Point(617, 252);
            this.pictureBox_Photo.Name = "pictureBox_Photo";
            this.pictureBox_Photo.Size = new System.Drawing.Size(170, 124);
            this.pictureBox_Photo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_Photo.TabIndex = 46;
            this.pictureBox_Photo.TabStop = false;
            this.pictureBox_Photo.Click += new System.EventHandler(this.pictureBox_Photo_Click);
            // 
            // button_Ajouter
            // 
            this.button_Ajouter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Ajouter.Image = global::AESMS.Properties.Resources.Button_Add_Icon_48;
            this.button_Ajouter.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button_Ajouter.Location = new System.Drawing.Point(261, 468);
            this.button_Ajouter.Name = "button_Ajouter";
            this.button_Ajouter.Size = new System.Drawing.Size(132, 75);
            this.button_Ajouter.TabIndex = 44;
            this.button_Ajouter.Text = "Ajouter";
            this.button_Ajouter.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button_Ajouter.UseVisualStyleBackColor = true;
            this.button_Ajouter.Click += new System.EventHandler(this.button_Ajouter_Click);
            // 
            // button_PreviewPrint
            // 
            this.button_PreviewPrint.Image = global::AESMS.Properties.Resources.Preview_Icon_48;
            this.button_PreviewPrint.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button_PreviewPrint.Location = new System.Drawing.Point(655, 171);
            this.button_PreviewPrint.Name = "button_PreviewPrint";
            this.button_PreviewPrint.Size = new System.Drawing.Size(132, 63);
            this.button_PreviewPrint.TabIndex = 41;
            this.button_PreviewPrint.Text = "Prévisualiser la carte";
            this.button_PreviewPrint.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button_PreviewPrint.UseVisualStyleBackColor = true;
            this.button_PreviewPrint.Click += new System.EventHandler(this.button_PreviewPrint_Click);
            // 
            // button_NouvelleAjout
            // 
            this.button_NouvelleAjout.Image = global::AESMS.Properties.Resources.Add_Male_User_Icon_48;
            this.button_NouvelleAjout.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button_NouvelleAjout.Location = new System.Drawing.Point(655, 84);
            this.button_NouvelleAjout.Name = "button_NouvelleAjout";
            this.button_NouvelleAjout.Size = new System.Drawing.Size(132, 63);
            this.button_NouvelleAjout.TabIndex = 40;
            this.button_NouvelleAjout.Text = "Nouvelle ajout";
            this.button_NouvelleAjout.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button_NouvelleAjout.UseVisualStyleBackColor = true;
            this.button_NouvelleAjout.Click += new System.EventHandler(this.button_NouvelleAjout_Click);
            // 
            // printPreviewDialog_CarteAdhesion
            // 
            this.printPreviewDialog_CarteAdhesion.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog_CarteAdhesion.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog_CarteAdhesion.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog_CarteAdhesion.Enabled = true;
            this.printPreviewDialog_CarteAdhesion.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog_CarteAdhesion.Icon")));
            this.printPreviewDialog_CarteAdhesion.Name = "printPreviewDialog_CarteAdhesion";
            this.printPreviewDialog_CarteAdhesion.Visible = false;
            // 
            // printDocument_CarteAdhesion
            // 
            this.printDocument_CarteAdhesion.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument_CarteAdhesion_PrintPage);
            // 
            // button_Retour
            // 
            this.button_Retour.BackgroundImage = global::AESMS.Properties.Resources.Alarm_Arrow_Left_Icon_48;
            this.button_Retour.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_Retour.Location = new System.Drawing.Point(12, 69);
            this.button_Retour.Name = "button_Retour";
            this.button_Retour.Size = new System.Drawing.Size(52, 51);
            this.button_Retour.TabIndex = 48;
            this.button_Retour.UseVisualStyleBackColor = true;
            this.button_Retour.Click += new System.EventHandler(this.button_Retour_Click);
            // 
            // button_Main
            // 
            this.button_Main.BackgroundImage = global::AESMS.Properties.Resources.bouton_home_menu_principal;
            this.button_Main.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_Main.Location = new System.Drawing.Point(12, 126);
            this.button_Main.Name = "button_Main";
            this.button_Main.Size = new System.Drawing.Size(52, 51);
            this.button_Main.TabIndex = 49;
            this.button_Main.UseVisualStyleBackColor = true;
            this.button_Main.Click += new System.EventHandler(this.button_Main_Click);
            // 
            // Formulaire_AjoutMembre
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::AESMS.Properties.Resources.bg;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(912, 555);
            this.Controls.Add(this.button_Main);
            this.Controls.Add(this.button_Retour);
            this.Controls.Add(this.button_Pivoter);
            this.Controls.Add(this.pictureBox_Photo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button_Ajouter);
            this.Controls.Add(this.button_PreviewPrint);
            this.Controls.Add(this.button_NouvelleAjout);
            this.Controls.Add(this.comboBox_Sexe);
            this.Controls.Add(this.label_Sexe);
            this.Controls.Add(this.dateTimePicker_DateNaissance);
            this.Controls.Add(this.textBox_Telephone);
            this.Controls.Add(this.label_Telephone);
            this.Controls.Add(this.textBox_Adresse);
            this.Controls.Add(this.textBox_Formation);
            this.Controls.Add(this.label_Formation);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label_DateNaissance);
            this.Controls.Add(this.textBox_Prenom);
            this.Controls.Add(this.label_Prenom);
            this.Controls.Add(this.textBox_Nom);
            this.Controls.Add(this.label_Nom);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Formulaire_AjoutMembre";
            this.Text = "Formulaire d\'ajout de membre";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Formulaire_AjoutMembre_FormClosed);
            this.Load += new System.EventHandler(this.Formulaire_AjoutMembre_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Photo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_Pivoter;
        private System.Windows.Forms.PictureBox pictureBox_Photo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button_Ajouter;
        private System.Windows.Forms.Button button_PreviewPrint;
        private System.Windows.Forms.Button button_NouvelleAjout;
        private System.Windows.Forms.ComboBox comboBox_Sexe;
        private System.Windows.Forms.Label label_Sexe;
        private System.Windows.Forms.DateTimePicker dateTimePicker_DateNaissance;
        private System.Windows.Forms.TextBox textBox_Telephone;
        private System.Windows.Forms.Label label_Telephone;
        private System.Windows.Forms.TextBox textBox_Adresse;
        private System.Windows.Forms.TextBox textBox_Formation;
        private System.Windows.Forms.Label label_Formation;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label_DateNaissance;
        private System.Windows.Forms.TextBox textBox_Prenom;
        private System.Windows.Forms.Label label_Prenom;
        private System.Windows.Forms.TextBox textBox_Nom;
        private System.Windows.Forms.Label label_Nom;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog_CarteAdhesion;
        private System.Drawing.Printing.PrintDocument printDocument_CarteAdhesion;
        private System.Windows.Forms.Button button_Retour;
        private System.Windows.Forms.Button button_Main;
    }
}