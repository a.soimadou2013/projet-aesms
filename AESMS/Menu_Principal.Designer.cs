﻿namespace AESMS
{
    partial class Menu_Principal
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Menu_Principal));
            this.button_GestionMembre = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button_GestionRessource = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button_GestionMembre
            // 
            this.button_GestionMembre.BackgroundImage = global::AESMS.Properties.Resources.User_group_Icon_128;
            this.button_GestionMembre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_GestionMembre.Location = new System.Drawing.Point(141, 24);
            this.button_GestionMembre.Name = "button_GestionMembre";
            this.button_GestionMembre.Size = new System.Drawing.Size(264, 143);
            this.button_GestionMembre.TabIndex = 0;
            this.button_GestionMembre.Text = "Gestion des membres";
            this.button_GestionMembre.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button_GestionMembre.UseVisualStyleBackColor = true;
            this.button_GestionMembre.Click += new System.EventHandler(this.button_GestionMembre_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(141, 361);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(264, 56);
            this.button1.TabIndex = 1;
            this.button1.Text = "Quitter";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button_GestionRessource
            // 
            this.button_GestionRessource.BackgroundImage = global::AESMS.Properties.Resources.Money_Icon_128;
            this.button_GestionRessource.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_GestionRessource.Location = new System.Drawing.Point(141, 173);
            this.button_GestionRessource.Name = "button_GestionRessource";
            this.button_GestionRessource.Size = new System.Drawing.Size(264, 143);
            this.button_GestionRessource.TabIndex = 2;
            this.button_GestionRessource.Text = "Gestion des ressources";
            this.button_GestionRessource.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button_GestionRessource.UseVisualStyleBackColor = true;
            this.button_GestionRessource.Click += new System.EventHandler(this.button_GestionRessource_Click);
            // 
            // Menu_Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::AESMS.Properties.Resources.bg;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(571, 513);
            this.Controls.Add(this.button_GestionRessource);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button_GestionMembre);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Menu_Principal";
            this.Text = "AESMS";
            this.Load += new System.EventHandler(this.Menu_Principal_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button_GestionMembre;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button_GestionRessource;
    }
}

