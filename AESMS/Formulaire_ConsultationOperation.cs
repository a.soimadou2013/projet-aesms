﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AESMS
{
    public partial class Formulaire_ConsultationOperation : Form
    {
        public Formulaire_ConsultationOperation()
        {
            InitializeComponent();
        }
        List<Operation> Liste_Lecture;
        
        int position = 0, nb = 0;
        Membre membreTrouve=new Membre();
        Operation opEnCour = new Operation();
        private void button_Suivant_Click(object sender, EventArgs e)
        {
            int i = 0;
            List<Operation> Lecture = Liste_Lecture;
            foreach (Operation op in Lecture)
            {

                if (i == position-1)
                {
                    textBox_ID.Text = "" + op.getId();
                    textBox_Date.Text = op.getDate().Day + "/" + op.getDate().Month + "/" + op.getDate().Year;
                    string type = "";
                    switch (op.getTypeOperation())
                    {
                        case 1:
                            type = "Dépot";
                            break;
                        case 2:
                            type = "Emprunt";
                            break;
                        case 3:
                            type = "Reboursement d'emprunt";
                            break;
                        case 4:
                            type = "Frais d'un événement";
                            break;
                        case 5:
                            type = "Autres frais supplémentaires";
                            break;
                        case 0:
                            type = "Aucune opération";
                            break;
                    }
                    textBox_TypeOperation.Text = type;
                    textBox_MontantOperation.Text = "" + op.getMontantOperation();
                    textBox_Restant.Text = "" + op.getMontantRestant();

                    if (textBox_TypeOperation.Text.Equals("Emprunt") && op.getEtatEmprunt())
                    {
                        button_MembreEmprunt.Visible = true;
                        button_RembourserEmprunt.Visible = true;
                    }
                    else
                    {
                        button_RembourserEmprunt.Visible = false;
                        button_MembreEmprunt.Visible = false;
                    }
                    membreTrouve = op.getMembre();
                    position =i;
                    opEnCour = op;

                    button_Precedant.Visible = true;
                    break;
                }
                
                i++;
                
            }
            if (position >= nb)
                button_Precedant.Visible = false;

            if (position <= 0)
            {
                button_Suivant.Visible = false;
            }
            else
            {
                button_Suivant.Visible = true;
            }
        }
        private void button_Precedant_Click(object sender, EventArgs e)
        {
            int i = 0;
            
            List<Operation> Lecture = Liste_Lecture;
            foreach (Operation op in Lecture)
            {

                if (i == position +1)
                {

                    textBox_ID.Text = "" + op.getId();
                    textBox_Date.Text = op.getDate().Day + "/" + op.getDate().Month + "/" + op.getDate().Year ;
                    string type = "";
                    switch (op.getTypeOperation())
                    {
                        case 1:
                            type = "Dépot";
                            break;
                        case 2:
                            type = "Emprunt";
                            break;
                        case 3:
                            type = "Reboursement d'emprunt";
                            break;
                        case 4:
                            type = "Frais d'un événement";
                            break;
                        case 5:
                            type = "Autres frais supplémentaires";
                            break;
                        case 0:
                            type = "Aucune opération";
                            break;
                    }
                    textBox_TypeOperation.Text = type;
                    textBox_MontantOperation.Text = "" + op.getMontantOperation();
                    textBox_Restant.Text = "" + op.getMontantRestant();
                    membreTrouve = op.getMembre();
                    if (textBox_TypeOperation.Text.Equals("Emprunt") && op.getEtatEmprunt())
                    {
                        button_MembreEmprunt.Visible = true;
                        button_RembourserEmprunt.Visible = true;
                    }                        
                    else
                    {
                        button_RembourserEmprunt.Visible = false;
                        button_MembreEmprunt.Visible = false;
                    }
                        
                    

                    position =i;
                    opEnCour = op;
                    break;
                }
               
                    i++;
            }
            
            if (position >= nb)
                button_Precedant.Visible = false;

            if (position <= 0)
            {
                button_Suivant.Visible = false;
            }
            else
            {
                button_Suivant.Visible = true;
            }

        }

        private void button_Retour_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button_Main_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        private void textBox_TypeOperation_TextChanged(object sender, EventArgs e)
        {
            
        }

        

        private void button_MembreEmprunt_Click(object sender, EventArgs e)
        {
            if (System.IO.File.Exists("MembreVeiw.aesms"))
                System.IO.File.Delete("MembreVeiw.aesms");

            IFormatter format = new BinaryFormatter();
            List<Membre> Liste_Trouver = new List<Membre>();
            Liste_Trouver.Add(membreTrouve);

            using (Stream flux = new FileStream("MembreVeiw.aesms", FileMode.Create, FileAccess.Write))
            {
                format.Serialize(flux, Liste_Trouver);
            }
            Formulaire_AfficherMembre affiche = new Formulaire_AfficherMembre();
            this.Hide();
            affiche.ShowDialog();
            this.Show();

            if (System.IO.File.Exists("MembreVeiw.aesms"))
                System.IO.File.Delete("MembreVeiw.aesms");
        }

        

        private void button_RembourserEmprunt_Click(object sender, EventArgs e)
        {
            IFormatter format = new BinaryFormatter();
            int MontantRemboursement = 0;
            string ch = textBox_MontantOperation.Text;
            MontantRemboursement = int.Parse(ch);
            bool continuer = true;
            if(MessageBox.Show("Voulez-vous vraiment rembourser cet emprunt ?","Confirmation",MessageBoxButtons.YesNo,MessageBoxIcon.Question)== DialogResult.Yes)
            {

                //Remboursement
                //etape 1: rendre false la variable qui determine l'etat de l'emprunt

                List<Operation> temp_Lecture = new List<Operation>();
                List<Operation> Liste_Lecture = new List<Operation>();
                Operation op_Last= new Operation();
                if (System.IO.File.Exists("operation.aesms"))
                {
                    //Lire le fichier operation.aesms et sauvegarder dans une liste 
                    using (Stream flux = new FileStream("operation.aesms", FileMode.Open, FileAccess.Read))
                    {
                        temp_Lecture = (List<Operation>)format.Deserialize(flux);
                    }
                    //Rechercher l'opération en cour et recupérer l'id du dérniere opération
                    foreach(Operation op in temp_Lecture)
                    {
                        op_Last = op;
                        if(opEnCour.getId().Equals(op.getId()))
                        {

                            //rendre false l'etat emprunt
                            op.setEtatEmprunt(false);
                            Liste_Lecture.Add(op);
                        }
                        else
                        {
                            Liste_Lecture.Add(op);
                        }
                    }
                    //Altérer le fichier ressources pour ajouter la somme emprunté
                    CompteBancaire compte = new CompteBancaire();
                    //Lire le fichier ressources
                    if (System.IO.File.Exists("ressources.aesms"))
                    {
                        using (Stream flux = new FileStream("ressources.aesms", FileMode.Open, FileAccess.Read))
                        {
                            compte = (CompteBancaire)format.Deserialize(flux);
                        }
                    }
                    else
                    {
                        continuer = false;
                        MessageBox.Show("Opération intérrompu, le fichier ressources.aesms est introuvable !", "Opération annulé", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    
                    compte.setMontant(compte.getMontant() + opEnCour.getMontantOperation());

                    //Enregistrer l'opération en cours modifié dans le membre en cours
                    if (System.IO.File.Exists("Membre.aesms"))
                    {

                        bool trouve = false;
                        List<Membre> ecriture = new List<Membre>();
                        using (Stream flux = new FileStream("Membre.aesms", FileMode.Open, FileAccess.Read))
                        {
                            List<Membre> Liste_Lecture_Membre;
                            List<Membre> Liste_Trouver = new List<Membre>();


                            Liste_Lecture_Membre = (List<Membre>)format.Deserialize(flux);
                            foreach(Membre m in Liste_Lecture_Membre)
                            {
                                if(membreTrouve.getCode().Equals(m.getCode()))
                                {
                                    List<Operation> Liste_Op = m.getOperationMembre();
                                    List<Operation> ecritureOp = new List<Operation>();
                                    foreach(Operation op in Liste_Op)
                                    {
                                        if(opEnCour.getId().Equals(op.getId()))
                                        {
                                            op.setEtatEmprunt(false);
                                            ecritureOp.Add(op);
                                        }
                                        else
                                        {
                                            ecritureOp.Add(op);
                                        }
                                    }
                                    opEnCour.setId(op_Last.getId() + 1);
                                    opEnCour.setTypeOperation(3);
                                    opEnCour.setMontantRestant(compte.getMontant());
                                    ecritureOp.Add(opEnCour);
                                    m.setOperationMembre(ecritureOp);
                                    Liste_Trouver.Add(m);
                                    trouve = true;
                                }
                                else
                                {
                                    Liste_Trouver.Add(m);
                                }
                            }
                            ecriture = Liste_Trouver;

                        }
                        if (trouve)
                        {
                            if (System.IO.File.Exists("Membre.aesms"))
                            {
                                System.IO.File.Delete("Membre.aesms");
                            }
                            using (Stream flux = new FileStream("Membre.aesms", FileMode.Create, FileAccess.Write))
                            {
                                format.Serialize(flux,ecriture);
                            }
                        }
                        else
                        {
                            continuer = false;
                            MessageBox.Show("Désolé, ce membre n'existe pas !", "Membre introuvable", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }


                    if(continuer)
                    {
                        //Enregistrement de l'opération de remboursement
                        Liste_Lecture.Add(opEnCour);

                        if (System.IO.File.Exists("ressources.aesms"))
                            System.IO.File.Delete("ressources.aesms");

                        using (Stream flux = new FileStream("ressources.aesms", FileMode.Create, FileAccess.Write))
                        {
                            format.Serialize(flux, compte);
                        }

                        if (System.IO.File.Exists("operation.aesms"))
                            System.IO.File.Delete("operation.aesms");
                        using (Stream flux = new FileStream("operation.aesms", FileMode.Create, FileAccess.Write))
                        {
                            format.Serialize(flux, Liste_Lecture);
                        }
                        MessageBox.Show("Remboursement éffectué avec succès !", "Emprunt éffectué", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                }
                else
                {
                    MessageBox.Show("Opération intérrompue, impossible de trouver le fichier opération.aesms","Opération annulé",MessageBoxButtons.OK,MessageBoxIcon.Error);
                }



            }
            else
            {
                MessageBox.Show("Remboursement annulé !", "Opération annulé", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void Formulaire_ConsultationOperation_Load(object sender, EventArgs e)
        {
            //
            IFormatter format = new BinaryFormatter();
            if (System.IO.File.Exists("operation.aesms"))
            {
                using (Stream flux = new FileStream("operation.aesms", FileMode.Open, FileAccess.Read))
                {
                    
                    int i = 0;
                    List<Operation> Lecture;
                    Liste_Lecture = Lecture = (List<Operation>)format.Deserialize(flux);
                    int cpt = 0;
                    foreach (Operation op in Lecture)
                    {
                        i++;
                        textBox_ID.Text = ""+op.getId();
                        textBox_Date.Text = op.getDate().Day + "/" + op.getDate().Month + "/" + op.getDate().Year ;
                        string type="";
                        switch (op.getTypeOperation())
                        {
                            case 1:
                                type = "Dépot";
                                break;
                            case 2:
                                type = "Emprunt";
                                break;
                            case 3:
                                type = "Reboursement d'emprunt";
                                break;
                            case 4:
                                type = "Frais d'un événement";
                                break;
                            case 5:
                                type = "Autres frais supplémentaires";
                                break;
                            case 0:
                                type = "Aucune opération";
                                break;
                        }
                        membreTrouve = op.getMembre();
                        opEnCour = op;
                        textBox_TypeOperation.Text = type;
                        textBox_MontantOperation.Text = ""+op.getMontantOperation();
                        textBox_Restant.Text = "" + op.getMontantRestant();
                        cpt++;

                    }
                    if (textBox_TypeOperation.Text.Equals("Emprunt") && opEnCour.getEtatEmprunt())
                    {
                        button_MembreEmprunt.Visible = true;
                        button_RembourserEmprunt.Visible = true;
                    }
                    else
                    {
                        button_RembourserEmprunt.Visible = false;
                        button_MembreEmprunt.Visible = false;
                    }
                    nb = i -1;
                    if (i > 1)
                    {
                        button_Suivant.Visible = true;
                    }
                    position=cpt-1;
                }
            }
            else
            {
                this.Close();
                MessageBox.Show("Désolé, veillez réaliser au moin une opération avant de continuer !", "Resultat Echec", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

        }
    }
}
