﻿namespace AESMS
{
    partial class Formulaire_AfficherMembre
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Formulaire_AfficherMembre));
            this.label_Sexe = new System.Windows.Forms.Label();
            this.textBox_Telephone = new System.Windows.Forms.TextBox();
            this.label_Telephone = new System.Windows.Forms.Label();
            this.textBox_Adresse = new System.Windows.Forms.TextBox();
            this.textBox_Formation = new System.Windows.Forms.TextBox();
            this.label_Formation = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label_DateNaissance = new System.Windows.Forms.Label();
            this.textBox_Prenom = new System.Windows.Forms.TextBox();
            this.label_Prenom = new System.Windows.Forms.Label();
            this.textBox_Nom = new System.Windows.Forms.TextBox();
            this.label_Nom = new System.Windows.Forms.Label();
            this.pictureBox_Photo = new System.Windows.Forms.PictureBox();
            this.textBox_DateNaissance = new System.Windows.Forms.TextBox();
            this.textBox_Sexe = new System.Windows.Forms.TextBox();
            this.button_Suivant = new System.Windows.Forms.Button();
            this.button_Precedant = new System.Windows.Forms.Button();
            this.button_GenererCarte = new System.Windows.Forms.Button();
            this.button_Retour = new System.Windows.Forms.Button();
            this.button_Main = new System.Windows.Forms.Button();
            this.button_AfficherOperation = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Photo)).BeginInit();
            this.SuspendLayout();
            // 
            // label_Sexe
            // 
            this.label_Sexe.AutoSize = true;
            this.label_Sexe.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label_Sexe.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label_Sexe.Location = new System.Drawing.Point(67, 171);
            this.label_Sexe.Name = "label_Sexe";
            this.label_Sexe.Size = new System.Drawing.Size(49, 20);
            this.label_Sexe.TabIndex = 52;
            this.label_Sexe.Text = "Sexe:";
            // 
            // textBox_Telephone
            // 
            this.textBox_Telephone.Enabled = false;
            this.textBox_Telephone.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBox_Telephone.Location = new System.Drawing.Point(249, 318);
            this.textBox_Telephone.Name = "textBox_Telephone";
            this.textBox_Telephone.Size = new System.Drawing.Size(153, 26);
            this.textBox_Telephone.TabIndex = 50;
            // 
            // label_Telephone
            // 
            this.label_Telephone.AutoSize = true;
            this.label_Telephone.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label_Telephone.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label_Telephone.Location = new System.Drawing.Point(67, 321);
            this.label_Telephone.Name = "label_Telephone";
            this.label_Telephone.Size = new System.Drawing.Size(112, 20);
            this.label_Telephone.TabIndex = 49;
            this.label_Telephone.Text = "No Téléphone:";
            // 
            // textBox_Adresse
            // 
            this.textBox_Adresse.Enabled = false;
            this.textBox_Adresse.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBox_Adresse.Location = new System.Drawing.Point(202, 267);
            this.textBox_Adresse.Name = "textBox_Adresse";
            this.textBox_Adresse.Size = new System.Drawing.Size(200, 26);
            this.textBox_Adresse.TabIndex = 48;
            // 
            // textBox_Formation
            // 
            this.textBox_Formation.Enabled = false;
            this.textBox_Formation.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBox_Formation.Location = new System.Drawing.Point(226, 219);
            this.textBox_Formation.Name = "textBox_Formation";
            this.textBox_Formation.Size = new System.Drawing.Size(176, 26);
            this.textBox_Formation.TabIndex = 46;
            // 
            // label_Formation
            // 
            this.label_Formation.AutoSize = true;
            this.label_Formation.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label_Formation.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label_Formation.Location = new System.Drawing.Point(67, 222);
            this.label_Formation.Name = "label_Formation";
            this.label_Formation.Size = new System.Drawing.Size(85, 20);
            this.label_Formation.TabIndex = 45;
            this.label_Formation.Text = "Formation:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label6.Location = new System.Drawing.Point(67, 270);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 20);
            this.label6.TabIndex = 47;
            this.label6.Text = "Adresse:";
            // 
            // label_DateNaissance
            // 
            this.label_DateNaissance.AutoSize = true;
            this.label_DateNaissance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label_DateNaissance.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label_DateNaissance.Location = new System.Drawing.Point(67, 125);
            this.label_DateNaissance.Name = "label_DateNaissance";
            this.label_DateNaissance.Size = new System.Drawing.Size(146, 20);
            this.label_DateNaissance.TabIndex = 44;
            this.label_DateNaissance.Text = "Date de naissance:";
            // 
            // textBox_Prenom
            // 
            this.textBox_Prenom.Enabled = false;
            this.textBox_Prenom.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBox_Prenom.Location = new System.Drawing.Point(226, 71);
            this.textBox_Prenom.Name = "textBox_Prenom";
            this.textBox_Prenom.Size = new System.Drawing.Size(176, 26);
            this.textBox_Prenom.TabIndex = 43;
            // 
            // label_Prenom
            // 
            this.label_Prenom.AutoSize = true;
            this.label_Prenom.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label_Prenom.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label_Prenom.Location = new System.Drawing.Point(67, 77);
            this.label_Prenom.Name = "label_Prenom";
            this.label_Prenom.Size = new System.Drawing.Size(68, 20);
            this.label_Prenom.TabIndex = 42;
            this.label_Prenom.Text = "Prénom:";
            // 
            // textBox_Nom
            // 
            this.textBox_Nom.Enabled = false;
            this.textBox_Nom.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBox_Nom.Location = new System.Drawing.Point(226, 22);
            this.textBox_Nom.Name = "textBox_Nom";
            this.textBox_Nom.Size = new System.Drawing.Size(176, 26);
            this.textBox_Nom.TabIndex = 41;
            // 
            // label_Nom
            // 
            this.label_Nom.AutoSize = true;
            this.label_Nom.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label_Nom.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label_Nom.Location = new System.Drawing.Point(67, 25);
            this.label_Nom.Name = "label_Nom";
            this.label_Nom.Size = new System.Drawing.Size(46, 20);
            this.label_Nom.TabIndex = 40;
            this.label_Nom.Text = "Nom:";
            // 
            // pictureBox_Photo
            // 
            this.pictureBox_Photo.BackgroundImage = global::AESMS.Properties.Resources.File_Pictures_Icon_256;
            this.pictureBox_Photo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox_Photo.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pictureBox_Photo.Location = new System.Drawing.Point(508, 12);
            this.pictureBox_Photo.Name = "pictureBox_Photo";
            this.pictureBox_Photo.Size = new System.Drawing.Size(210, 166);
            this.pictureBox_Photo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_Photo.TabIndex = 54;
            this.pictureBox_Photo.TabStop = false;
            // 
            // textBox_DateNaissance
            // 
            this.textBox_DateNaissance.Enabled = false;
            this.textBox_DateNaissance.Location = new System.Drawing.Point(249, 125);
            this.textBox_DateNaissance.Name = "textBox_DateNaissance";
            this.textBox_DateNaissance.Size = new System.Drawing.Size(153, 20);
            this.textBox_DateNaissance.TabIndex = 55;
            // 
            // textBox_Sexe
            // 
            this.textBox_Sexe.Enabled = false;
            this.textBox_Sexe.Location = new System.Drawing.Point(280, 171);
            this.textBox_Sexe.Name = "textBox_Sexe";
            this.textBox_Sexe.Size = new System.Drawing.Size(122, 20);
            this.textBox_Sexe.TabIndex = 56;
            // 
            // button_Suivant
            // 
            this.button_Suivant.Image = global::AESMS.Properties.Resources.Alarm_Arrow_Right_Icon_96;
            this.button_Suivant.Location = new System.Drawing.Point(597, 282);
            this.button_Suivant.Name = "button_Suivant";
            this.button_Suivant.Size = new System.Drawing.Size(121, 100);
            this.button_Suivant.TabIndex = 57;
            this.button_Suivant.UseVisualStyleBackColor = true;
            this.button_Suivant.Visible = false;
            this.button_Suivant.Click += new System.EventHandler(this.button_Suivant_Click);
            // 
            // button_Precedant
            // 
            this.button_Precedant.Image = global::AESMS.Properties.Resources.Alarm_Arrow_Left_Icon_96;
            this.button_Precedant.Location = new System.Drawing.Point(470, 282);
            this.button_Precedant.Name = "button_Precedant";
            this.button_Precedant.Size = new System.Drawing.Size(121, 100);
            this.button_Precedant.TabIndex = 58;
            this.button_Precedant.UseVisualStyleBackColor = true;
            this.button_Precedant.Visible = false;
            this.button_Precedant.Click += new System.EventHandler(this.button_Precedant_Click);
            // 
            // button_GenererCarte
            // 
            this.button_GenererCarte.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_GenererCarte.Location = new System.Drawing.Point(508, 202);
            this.button_GenererCarte.Name = "button_GenererCarte";
            this.button_GenererCarte.Size = new System.Drawing.Size(210, 62);
            this.button_GenererCarte.TabIndex = 59;
            this.button_GenererCarte.Text = "Génerer la carte d\'adhésion";
            this.button_GenererCarte.UseVisualStyleBackColor = true;
            this.button_GenererCarte.Click += new System.EventHandler(this.button_GenererCarte_Click);
            // 
            // button_Retour
            // 
            this.button_Retour.BackgroundImage = global::AESMS.Properties.Resources.Alarm_Arrow_Left_Icon_48;
            this.button_Retour.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_Retour.Location = new System.Drawing.Point(9, 12);
            this.button_Retour.Name = "button_Retour";
            this.button_Retour.Size = new System.Drawing.Size(52, 51);
            this.button_Retour.TabIndex = 60;
            this.button_Retour.UseVisualStyleBackColor = true;
            this.button_Retour.Click += new System.EventHandler(this.button_Retour_Click);
            // 
            // button_Main
            // 
            this.button_Main.BackgroundImage = global::AESMS.Properties.Resources.bouton_home_menu_principal;
            this.button_Main.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_Main.Location = new System.Drawing.Point(9, 69);
            this.button_Main.Name = "button_Main";
            this.button_Main.Size = new System.Drawing.Size(52, 51);
            this.button_Main.TabIndex = 61;
            this.button_Main.UseVisualStyleBackColor = true;
            this.button_Main.Click += new System.EventHandler(this.button_Main_Click);
            // 
            // button_AfficherOperation
            // 
            this.button_AfficherOperation.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_AfficherOperation.ForeColor = System.Drawing.Color.Maroon;
            this.button_AfficherOperation.Location = new System.Drawing.Point(71, 359);
            this.button_AfficherOperation.Name = "button_AfficherOperation";
            this.button_AfficherOperation.Size = new System.Drawing.Size(231, 61);
            this.button_AfficherOperation.TabIndex = 62;
            this.button_AfficherOperation.Text = "Afficher les opérations effectué par ce membre";
            this.button_AfficherOperation.UseVisualStyleBackColor = true;
            this.button_AfficherOperation.Click += new System.EventHandler(this.button_AfficherOperation_Click);
            // 
            // Formulaire_AfficherMembre
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::AESMS.Properties.Resources.bg;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(752, 432);
            this.Controls.Add(this.button_AfficherOperation);
            this.Controls.Add(this.button_Main);
            this.Controls.Add(this.button_Retour);
            this.Controls.Add(this.button_GenererCarte);
            this.Controls.Add(this.button_Precedant);
            this.Controls.Add(this.button_Suivant);
            this.Controls.Add(this.textBox_Sexe);
            this.Controls.Add(this.textBox_DateNaissance);
            this.Controls.Add(this.pictureBox_Photo);
            this.Controls.Add(this.label_Sexe);
            this.Controls.Add(this.textBox_Telephone);
            this.Controls.Add(this.label_Telephone);
            this.Controls.Add(this.textBox_Adresse);
            this.Controls.Add(this.textBox_Formation);
            this.Controls.Add(this.label_Formation);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label_DateNaissance);
            this.Controls.Add(this.textBox_Prenom);
            this.Controls.Add(this.label_Prenom);
            this.Controls.Add(this.textBox_Nom);
            this.Controls.Add(this.label_Nom);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Formulaire_AfficherMembre";
            this.Text = "Affichage de membre";
            this.Load += new System.EventHandler(this.Formulaire_AfficherMembre_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Photo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label_Sexe;
        private System.Windows.Forms.TextBox textBox_Telephone;
        private System.Windows.Forms.Label label_Telephone;
        private System.Windows.Forms.TextBox textBox_Adresse;
        private System.Windows.Forms.TextBox textBox_Formation;
        private System.Windows.Forms.Label label_Formation;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label_DateNaissance;
        private System.Windows.Forms.TextBox textBox_Prenom;
        private System.Windows.Forms.Label label_Prenom;
        private System.Windows.Forms.TextBox textBox_Nom;
        private System.Windows.Forms.Label label_Nom;
        private System.Windows.Forms.PictureBox pictureBox_Photo;
        private System.Windows.Forms.TextBox textBox_DateNaissance;
        private System.Windows.Forms.TextBox textBox_Sexe;
        private System.Windows.Forms.Button button_Suivant;
        private System.Windows.Forms.Button button_Precedant;
        private System.Windows.Forms.Button button_GenererCarte;
        private System.Windows.Forms.Button button_Retour;
        private System.Windows.Forms.Button button_Main;
        private System.Windows.Forms.Button button_AfficherOperation;
    }
}