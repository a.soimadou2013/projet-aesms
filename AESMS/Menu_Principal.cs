﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AESMS
{
    public partial class Menu_Principal : Form
    {
        public Menu_Principal()
        {
            InitializeComponent();
        }

        private void Menu_Principal_Load(object sender, EventArgs e)
        {
            //
            int i = 0;
            IFormatter format = new BinaryFormatter();
            List<Membre> Liste_Trouver = new List<Membre>();

            if (System.IO.File.Exists("operation.aesms"))
            {
                using (Stream flux = new FileStream("operation.aesms", FileMode.Open, FileAccess.Read))
                {

                    
                    List<Operation> Lecture;
                    Lecture = (List<Operation>)format.Deserialize(flux);

                    foreach (Operation op in Lecture)
                    {
                        if(op.getTypeOperation()==2 && op.getEtatEmprunt()==true)
                        {
                            List<Membre> temp = Liste_Trouver;
                            bool ok = false;
                            foreach(Membre op2 in temp)
                            {
                                if (op.getMembre().getCode().Equals(op2.getCode()))
                                {
                                    ok = true;
                                    break;
                                }
                                    
                            }
                            if(ok==false)
                            {
                                if(System.IO.File.Exists("Membre.aesms"))
                                {
                                    List<Membre> Liste_Membre;
                                    Membre membreTrouve = new Membre();
                                    using (Stream flux2 = new FileStream("Membre.aesms", FileMode.Open, FileAccess.Read))
                                    {
                                        Liste_Membre= (List<Membre>)format.Deserialize(flux2);
                                    }
                                    
                                    foreach(Membre m in Liste_Membre)
                                    {
                                        if(op.getMembre().getCode().Equals(m.getCode()))
                                        {
                                            membreTrouve = m;
                                        }
                                    }
                                    Liste_Trouver.Add(membreTrouve);
                                    i++;
                                }
                                
                            }
                            
                        }
                    }
                }
                if (i > 0)
                {
                    if(MessageBox.Show("Il y'a "+i+" emprunt(s) non remboursé(s), voulez-vous consulter ces membres ?", "Rappel emprunt", MessageBoxButtons.YesNo, MessageBoxIcon.Information)== DialogResult.Yes)
                    {
                        if (System.IO.File.Exists("MembreVeiw.aesms"))
                            System.IO.File.Delete("MembreVeiw.aesms");

                        
                        

                        using (Stream flux = new FileStream("MembreVeiw.aesms", FileMode.Create, FileAccess.Write))
                        {
                            format.Serialize(flux, Liste_Trouver);
                        }
                        Formulaire_AfficherMembre affiche = new Formulaire_AfficherMembre();
                        this.Hide();
                        affiche.ShowDialog();
                        this.Show();

                        
                    }
                }
                    
            }
            

        }

        private void button_GestionMembre_Click(object sender, EventArgs e)
        {
            SousMenu_GestionMembre Gest_membre = new SousMenu_GestionMembre();
            this.Hide();
            Gest_membre.ShowDialog();
            this.Show();
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button_GestionRessource_Click(object sender, EventArgs e)
        {
            SousMenu_GestionRessources ressources = new SousMenu_GestionRessources();
            this.Hide();
            ressources.ShowDialog();
            this.Show();
        }
    }
}
