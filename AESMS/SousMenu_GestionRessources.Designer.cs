﻿namespace AESMS
{
    partial class SousMenu_GestionRessources
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SousMenu_GestionRessources));
            this.button_ConsulterSolde = new System.Windows.Forms.Button();
            this.button_Main = new System.Windows.Forms.Button();
            this.button_Retour = new System.Windows.Forms.Button();
            this.button_EffectuerDepot = new System.Windows.Forms.Button();
            this.button_EffectuerEmprunt = new System.Windows.Forms.Button();
            this.button_ConsulterOperation = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button_ConsulterSolde
            // 
            this.button_ConsulterSolde.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.button_ConsulterSolde.BackgroundImage = global::AESMS.Properties.Resources.Money_Safe_Icon_96;
            this.button_ConsulterSolde.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button_ConsulterSolde.Cursor = System.Windows.Forms.Cursors.Default;
            this.button_ConsulterSolde.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_ConsulterSolde.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button_ConsulterSolde.Location = new System.Drawing.Point(85, 26);
            this.button_ConsulterSolde.Name = "button_ConsulterSolde";
            this.button_ConsulterSolde.Size = new System.Drawing.Size(203, 130);
            this.button_ConsulterSolde.TabIndex = 1;
            this.button_ConsulterSolde.Text = "Consulter le solde du compte";
            this.button_ConsulterSolde.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button_ConsulterSolde.UseVisualStyleBackColor = false;
            this.button_ConsulterSolde.Click += new System.EventHandler(this.button_ConsulterSolde_Click);
            // 
            // button_Main
            // 
            this.button_Main.BackgroundImage = global::AESMS.Properties.Resources.bouton_home_menu_principal;
            this.button_Main.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_Main.Location = new System.Drawing.Point(12, 71);
            this.button_Main.Name = "button_Main";
            this.button_Main.Size = new System.Drawing.Size(52, 51);
            this.button_Main.TabIndex = 8;
            this.button_Main.UseVisualStyleBackColor = true;
            this.button_Main.Click += new System.EventHandler(this.button_Main_Click);
            // 
            // button_Retour
            // 
            this.button_Retour.BackgroundImage = global::AESMS.Properties.Resources.Alarm_Arrow_Left_Icon_48;
            this.button_Retour.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_Retour.Location = new System.Drawing.Point(12, 14);
            this.button_Retour.Name = "button_Retour";
            this.button_Retour.Size = new System.Drawing.Size(52, 51);
            this.button_Retour.TabIndex = 7;
            this.button_Retour.UseVisualStyleBackColor = true;
            this.button_Retour.Click += new System.EventHandler(this.button_Retour_Click);
            // 
            // button_EffectuerDepot
            // 
            this.button_EffectuerDepot.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.button_EffectuerDepot.BackgroundImage = global::AESMS.Properties.Resources.Safe_Icon_96;
            this.button_EffectuerDepot.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button_EffectuerDepot.Cursor = System.Windows.Forms.Cursors.Default;
            this.button_EffectuerDepot.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_EffectuerDepot.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button_EffectuerDepot.Location = new System.Drawing.Point(400, 26);
            this.button_EffectuerDepot.Name = "button_EffectuerDepot";
            this.button_EffectuerDepot.Size = new System.Drawing.Size(203, 130);
            this.button_EffectuerDepot.TabIndex = 9;
            this.button_EffectuerDepot.Text = "Effectuer un dépot dans le compte";
            this.button_EffectuerDepot.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button_EffectuerDepot.UseVisualStyleBackColor = false;
            this.button_EffectuerDepot.Click += new System.EventHandler(this.button_EffectuerDepot_Click);
            // 
            // button_EffectuerEmprunt
            // 
            this.button_EffectuerEmprunt.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.button_EffectuerEmprunt.BackgroundImage = global::AESMS.Properties.Resources.Money_Safe_1_Icon_96;
            this.button_EffectuerEmprunt.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button_EffectuerEmprunt.Cursor = System.Windows.Forms.Cursors.Default;
            this.button_EffectuerEmprunt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_EffectuerEmprunt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button_EffectuerEmprunt.Location = new System.Drawing.Point(85, 304);
            this.button_EffectuerEmprunt.Name = "button_EffectuerEmprunt";
            this.button_EffectuerEmprunt.Size = new System.Drawing.Size(203, 130);
            this.button_EffectuerEmprunt.TabIndex = 10;
            this.button_EffectuerEmprunt.Text = "Effectuer un emprunt pour un membre";
            this.button_EffectuerEmprunt.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button_EffectuerEmprunt.UseVisualStyleBackColor = false;
            this.button_EffectuerEmprunt.Click += new System.EventHandler(this.button_EffectuerEmprunt_Click);
            // 
            // button_ConsulterOperation
            // 
            this.button_ConsulterOperation.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.button_ConsulterOperation.BackgroundImage = global::AESMS.Properties.Resources.Money_Calculator_Icon_96;
            this.button_ConsulterOperation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button_ConsulterOperation.Cursor = System.Windows.Forms.Cursors.Default;
            this.button_ConsulterOperation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_ConsulterOperation.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button_ConsulterOperation.Location = new System.Drawing.Point(400, 304);
            this.button_ConsulterOperation.Name = "button_ConsulterOperation";
            this.button_ConsulterOperation.Size = new System.Drawing.Size(203, 130);
            this.button_ConsulterOperation.TabIndex = 11;
            this.button_ConsulterOperation.Text = "Consulter les Opérations";
            this.button_ConsulterOperation.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button_ConsulterOperation.UseVisualStyleBackColor = false;
            this.button_ConsulterOperation.Click += new System.EventHandler(this.button_ConsulterOperation_Click);
            // 
            // SousMenu_GestionRessources
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::AESMS.Properties.Resources.bg;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(646, 458);
            this.Controls.Add(this.button_ConsulterOperation);
            this.Controls.Add(this.button_EffectuerEmprunt);
            this.Controls.Add(this.button_EffectuerDepot);
            this.Controls.Add(this.button_Main);
            this.Controls.Add(this.button_Retour);
            this.Controls.Add(this.button_ConsulterSolde);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SousMenu_GestionRessources";
            this.Text = "Menu Gestion des ressources";
            this.Load += new System.EventHandler(this.SousMenu_GestionRessources_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button_ConsulterSolde;
        private System.Windows.Forms.Button button_Main;
        private System.Windows.Forms.Button button_Retour;
        private System.Windows.Forms.Button button_EffectuerDepot;
        private System.Windows.Forms.Button button_EffectuerEmprunt;
        private System.Windows.Forms.Button button_ConsulterOperation;
    }
}