﻿namespace AESMS
{
    partial class Formulaire_Emprunt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Formulaire_Emprunt));
            this.button_Continuer = new System.Windows.Forms.Button();
            this.comboBox_ListeMembre = new System.Windows.Forms.ComboBox();
            this.button_Main = new System.Windows.Forms.Button();
            this.button_Retour = new System.Windows.Forms.Button();
            this.label_MontantEmprunt = new System.Windows.Forms.Label();
            this.textBox_MontantEmprunt = new System.Windows.Forms.TextBox();
            this.button_EffectuerEmprunt = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button_Continuer
            // 
            this.button_Continuer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Continuer.Location = new System.Drawing.Point(266, 181);
            this.button_Continuer.Name = "button_Continuer";
            this.button_Continuer.Size = new System.Drawing.Size(127, 53);
            this.button_Continuer.TabIndex = 9;
            this.button_Continuer.Text = "Continuer";
            this.button_Continuer.UseVisualStyleBackColor = true;
            this.button_Continuer.Click += new System.EventHandler(this.button_Continuer_Click);
            // 
            // comboBox_ListeMembre
            // 
            this.comboBox_ListeMembre.FormattingEnabled = true;
            this.comboBox_ListeMembre.Location = new System.Drawing.Point(155, 47);
            this.comboBox_ListeMembre.Name = "comboBox_ListeMembre";
            this.comboBox_ListeMembre.Size = new System.Drawing.Size(365, 21);
            this.comboBox_ListeMembre.TabIndex = 8;
            this.comboBox_ListeMembre.Text = "Liste des membres de l\'association AESMS";
            // 
            // button_Main
            // 
            this.button_Main.BackgroundImage = global::AESMS.Properties.Resources.bouton_home_menu_principal;
            this.button_Main.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_Main.Location = new System.Drawing.Point(70, 12);
            this.button_Main.Name = "button_Main";
            this.button_Main.Size = new System.Drawing.Size(52, 51);
            this.button_Main.TabIndex = 11;
            this.button_Main.UseVisualStyleBackColor = true;
            this.button_Main.Click += new System.EventHandler(this.button_Main_Click);
            // 
            // button_Retour
            // 
            this.button_Retour.BackgroundImage = global::AESMS.Properties.Resources.Alarm_Arrow_Left_Icon_48;
            this.button_Retour.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_Retour.Location = new System.Drawing.Point(12, 12);
            this.button_Retour.Name = "button_Retour";
            this.button_Retour.Size = new System.Drawing.Size(52, 51);
            this.button_Retour.TabIndex = 10;
            this.button_Retour.UseVisualStyleBackColor = true;
            this.button_Retour.Click += new System.EventHandler(this.button_Retour_Click);
            // 
            // label_MontantEmprunt
            // 
            this.label_MontantEmprunt.AutoSize = true;
            this.label_MontantEmprunt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_MontantEmprunt.Location = new System.Drawing.Point(110, 91);
            this.label_MontantEmprunt.Name = "label_MontantEmprunt";
            this.label_MontantEmprunt.Size = new System.Drawing.Size(256, 20);
            this.label_MontantEmprunt.TabIndex = 12;
            this.label_MontantEmprunt.Text = "Entrer le montant à emprunter:";
            this.label_MontantEmprunt.Visible = false;
            // 
            // textBox_MontantEmprunt
            // 
            this.textBox_MontantEmprunt.Location = new System.Drawing.Point(391, 91);
            this.textBox_MontantEmprunt.Name = "textBox_MontantEmprunt";
            this.textBox_MontantEmprunt.Size = new System.Drawing.Size(174, 20);
            this.textBox_MontantEmprunt.TabIndex = 13;
            this.textBox_MontantEmprunt.Text = "1";
            this.textBox_MontantEmprunt.Visible = false;
            // 
            // button_EffectuerEmprunt
            // 
            this.button_EffectuerEmprunt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_EffectuerEmprunt.Location = new System.Drawing.Point(266, 181);
            this.button_EffectuerEmprunt.Name = "button_EffectuerEmprunt";
            this.button_EffectuerEmprunt.Size = new System.Drawing.Size(127, 53);
            this.button_EffectuerEmprunt.TabIndex = 14;
            this.button_EffectuerEmprunt.Text = "Effectuer l\'emprunt !";
            this.button_EffectuerEmprunt.UseVisualStyleBackColor = true;
            this.button_EffectuerEmprunt.Click += new System.EventHandler(this.button_EffectuerEmprunt_Click);
            // 
            // Formulaire_Emprunt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::AESMS.Properties.Resources.bg;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(701, 261);
            this.Controls.Add(this.button_EffectuerEmprunt);
            this.Controls.Add(this.textBox_MontantEmprunt);
            this.Controls.Add(this.label_MontantEmprunt);
            this.Controls.Add(this.button_Main);
            this.Controls.Add(this.button_Retour);
            this.Controls.Add(this.button_Continuer);
            this.Controls.Add(this.comboBox_ListeMembre);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Formulaire_Emprunt";
            this.Text = "Formulaire d\'emprunt dans la caisse de l\'association";
            this.Load += new System.EventHandler(this.Formulaire_Emprunt_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_Main;
        private System.Windows.Forms.Button button_Retour;
        private System.Windows.Forms.Button button_Continuer;
        private System.Windows.Forms.ComboBox comboBox_ListeMembre;
        private System.Windows.Forms.Label label_MontantEmprunt;
        private System.Windows.Forms.TextBox textBox_MontantEmprunt;
        private System.Windows.Forms.Button button_EffectuerEmprunt;
    }
}